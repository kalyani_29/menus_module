## Documention

## About Menus Module

Following are the files which are related to the Menus Module &the required Common Files for Menu module to execute individually.

## Controllers :
- 	MenusController
-	MenusReportController
-	ExportController -> Common File for Export functionality

## Base:
-	MenusBaseController

## Controller_services :
-	MenusServiceController.php

## Model Folder:
-	Menus.php
-	MenusMapping.php
-	RoleManagement.php
-	RoleModel.php -> Common Model File for roles.
-   MasterModel.php -> Functions (insertData, updateData, getMasterRow, getMaster ,		
	                   queryBinder) shift this function from menus.php to MasterModel.php 

## Database Table:
-	menus
-	menu_mapping
-	role_management
-	roles  ->Common table required for roles list in access control.
-	company->common table to list out company in access control.
-	general_master->common table to load Company list.

## Migrations :
-	menus.php
-	menu_mapping.php
-	role_management.php
-   menu_mapping_after_update_trigger.php
-   insert_general_maser_log.php
-   log_table.php
-   get_host_name.php

## Views:
-	Layout  :This layouts is common layouts. use in  MenusReport. 
-	app.blade.php
-	listing-container.blade.php
-	listing.blade.php
-	search.blade.php
-	pagination.blade.php

-	super_admin/menus :This views use in menus Module.
-	index.blade.php
-	access_control.blade.php


## Common Folder:
-	common.php ->get_translation(); get_short_string();dropDownList function(); These functions is
                 used in MenusController Module

## Export Folder: 
-	Export.php  ->files is common file used in errorlog Module and Menus Module.

## Images used for Menu Models:
-	 chosen-sprite.css

## Public/js/menu/js:
-	1.11.3_jquery.min.js
-	1.12.1_jquery-ui.js
-	3.3.6_bootstrap.min.js
-	Jquery.nestable.js
-	Jquery.nestable++.js


## CSS Used for Menu Models:
-	accordion.css
-	app.css
-	bootstrap.min.css
-	chosen.css
-	daterangepicker.css
-	menu.css
-	orange_theme.css


## JS used for Menu models: 
-	accordion.js
-	bootstrap.min.js
-	chosen.jquery.js
-	common.js
-	daterangepicker.min.js
-	jquery.min.js
-	jquery.nestable.js
-	menu.js
-	moment.min.js
-	tiny.min.js.









