-- ------------------------------------------------------------------------------
----------------------------Trigger Function " GetHostName  "---------------------------
--  1. F U N C T I O N
--
DELIMITER $$
CREATE FUNCTION `GetHostName` () RETURNS VARCHAR(255) CHARSET latin1 BEGIN

    DECLARE local_hostname VARCHAR(255);

    SELECT variable_value INTO local_hostname
    FROM information_schema.global_variables
    WHERE variable_name = 'hostname';

    RETURN local_hostname;

END;
$$

DELIMITER ;

--------------------------Trigger ---------------------------------------------------------------
----------2] Function " insert_general_master_log "-------------------------------------
--------
DELIMITER $$
CREATE FUNCTION `insert_general_master_log`(`table_name` VARCHAR(20),
 `column_name` VARCHAR(20), `previous_value` VARCHAR(255), `updated_value` VARCHAR(255),
 `db_user` VARCHAR(255), `ref_id` VARCHAR(255), `ip_address` VARCHAR(255), `device` VARCHAR(255),
 `created_by` INT(11), `updated_by` INT(11)) RETURNS VARCHAR(10) CHARSET latin1
BEGIN
  DECLARE INSERT_STATUS VARCHAR(255);
    INSERT INTO `log_table`(
            `table_name`,
            `column_name`,
            `previous_value`,
            `updated_value`,
            `db_user`,
            `ref_id`,
            `ip_address`,
            `localhost_name`,
            `device`,
            `created_by`,
            `updated_by`,
            `created_at`,
            `updated_at`
        )
        VALUES(
             table_name
            ,column_name
            ,previous_value
            ,updated_value
            ,CURRENT_USER()
            ,ref_id
            ,ip_address
            ,GetHostName()
            ,device
            ,created_by
            ,updated_by
            ,CURRENT_TIMESTAMP
            ,CURRENT_TIMESTAMP
        );
       SET INSERT_STATUS = "Log Created";
       RETURN (INSERT_STATUS);
END;
$$

DELIMITER ;


------------------------------------------------------------------------------------------------------
-------------------------- Trigger Creation " menu_mapping_after_update_trigger " -------------------------------------------------------
-------------
--------
DELIMITER $$
CREATE TRIGGER `menu_mapping_after_update_trigger` AFTER UPDATE ON `menu_mapping` FOR EACH ROW BEGIN
  DECLARE GM_STATUS VARCHAR(255);
  IF NEW.menu_master_id <> OLD.menu_master_id         THEN
    SET GM_STATUS = insert_general_master_log('menu_mapping','menu_master_id',old.menu_master_id,new.menu_master_id,CURRENT_USER,old.id,'192.168.43.1','W','0','0');
  ELSEIF  NEW.menu_id <> OLD.menu_id   THEN
    SET GM_STATUS = insert_general_master_log('menu_mapping','menu_id',old.menu_id,new.menu_id,CURRENT_USER,old.id,'192.168.43.1','W','0','0');
  ELSEIF NEW.parent_id <> OLD.parent_id       THEN
    SET GM_STATUS = insert_general_master_log('menu_mapping','parent_id',old.parent_id,new.parent_id,CURRENT_USER,old.id,'192.168.43.1','W','0','0');
   ELSEIF NEW.reference_id <> OLD.reference_id       THEN
    SET GM_STATUS = insert_general_master_log('menu_mapping','reference_id',old.reference_id,new.reference_id,CURRENT_USER,old.id,'192.168.43.1','W','0','0');
   ELSEIF NEW.short_code <> OLD.short_code       THEN
    SET GM_STATUS = insert_general_master_log('menu_mapping','short_code',old.short_code,new.short_code,CURRENT_USER,old.id,'192.168.43.1','W','0','0');
   ELSEIF NEW.sequence <> OLD.sequence       THEN
    SET GM_STATUS = insert_general_master_log('menu_mapping','sequence',old.sequence,new.sequence,CURRENT_USER,old.id,'192.168.43.1','W','0','0');
   ELSEIF NEW.description <> OLD.description       THEN
    SET GM_STATUS = insert_general_master_log('menu_mapping','description',old.description,new.description,CURRENT_USER,old.id,'192.168.43.1','W','0','0');
   ELSEIF NEW.url <> OLD.url       THEN
    SET GM_STATUS = insert_general_master_log('menu_mapping','url',old.url,new.url,CURRENT_USER,old.id,'192.168.43.1','W','0','0');
   ELSEIF NEW.class <> OLD.class       THEN
    SET GM_STATUS = insert_general_master_log('menu_mapping','class',old.class,new.class,CURRENT_USER,old.id,'192.168.43.1','W','0','0');
   ELSEIF NEW.font_icon_class <> OLD.font_icon_class       THEN
    SET GM_STATUS = insert_general_master_log('menu_mapping','font_icon_class',old.font_icon_class,new.font_icon_class,CURRENT_USER,old.id,'192.168.43.1','W','0','0');
   ELSEIF NEW.is_deleted <> OLD.is_deleted       THEN
    SET GM_STATUS = insert_general_master_log('menu_mapping','is_deleted',old.is_deleted,new.is_deleted,CURRENT_USER,old.id,'192.168.43.1','W','0','0');
  END IF;
END
$$
DELIMITER ;
