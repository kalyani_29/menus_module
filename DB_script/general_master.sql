CREATE TABLE `general_master` (
  `id` int(11) NOT NULL,
  `company_id` int(11) NOT NULL,
  `gen_id` varchar(20) NOT NULL COMMENT 'Combination of master_code+code',
  `code` varchar(20) NOT NULL,
  `value` varchar(50) DEFAULT NULL,
  `description` varchar(100) NOT NULL,
  `master_id` int(11) DEFAULT NULL,
  `master_code` varchar(50) NOT NULL,
  `master_description` varchar(100) NOT NULL,
  `parent_id` varchar(20) DEFAULT NULL,
  `sequence` int(11) DEFAULT NULL,
  `locale_code` varchar(150) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_by` int(11) NOT NULL,
  `updated_by` int(11) NOT NULL,
  `created_at` datetime DEFAULT current_timestamp(),
  `updated_at` datetime DEFAULT current_timestamp(),
  `is_deleted` tinyint(1) NOT NULL DEFAULT 0,
  `is_completed` tinyint(1) NOT NULL DEFAULT 0,
  `locked` int(11) DEFAULT 0 COMMENT '0 -> No, 1 -> Yes'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;


/** general_master table
  insert record in generalmaster table where master_code = MENU
    kalyani 8-1-2022
**/

INSERT INTO `general_master` (`id`, `company_id`, `gen_id`, `code`, `value`, `description`, `master_id`, `master_code`, `master_description`, `parent_id`, `sequence`, `locale_code`, `created_by`, `updated_by`, `created_at`, `updated_at`, `is_deleted`, `is_completed`, `locked`) VALUES
(1, 0, 'MENUSUP-TOP', 'SUP-TOP', NULL, 'Super Admin Menu', 1, 'MENU', 'Menu Master', 'NULL', 1, 'EN', 1, 1, '2020-01-26 13:20:24', '2020-01-26 13:20:24', 0, 0, 0),
(2, 0, 'MENUCOLTOP', 'COLTOP', NULL, 'Collab Admin Menu', 1, 'MENU', 'Menu Master', 'NULL', 2, 'EN', 1, 1, '2020-01-26 13:20:24', '2020-01-26 13:20:24', 0, 0, 0),
(3, 0, 'MENUUSER', 'USER', NULL, 'User Menu', 1, 'MENU', 'Menu Master', 'NULL', 3, 'EN', 1, 1, '2020-01-26 13:20:24', '2020-01-26 13:20:24', 0, 0, 0),
(7, 0, 'MENUCOLLAB-TEAM', 'COLLAB-TEAM', NULL, 'Collab Team', 1, 'MENU', 'Menu Master', NULL, 4, 'EN', 1, 1, '2020-02-05 19:02:30', '2020-02-05 19:02:30', 0, 0, 0),
(23, 0, 'MENUACCO_MENU', 'ACCO_MENU', NULL, 'Accordion Menu', 1, 'MENU', 'Menu Master', '', 5, 'EN', 1, 1, '2020-02-21 13:43:02', '2020-02-21 13:43:02', 0, 0, 0),
(406, 0, 'MENURPT	RPT', 'RPT', NULL, 'Report', 1, 'MENU', 'Menu Master', '', 6, 'EN', 1, 1, '2020-05-11 12:44:03', '2020-05-11 12:44:03', 0, 0, 0),
(407, 0, 'MENUADM', 'ADM', NULL, 'Data Operation', 1, 'MENU', 'Menu Master', '', 7, 'EN', 1, 1, '2020-05-11 12:44:03', '2020-05-11 12:44:03', 0, 0, 0),
(408, 0, 'USER_DASHBOARD', 'USER_DASH', NULL, 'User Dashboard', 1, 'MENU', 'Menu Master', '', 8, 'EN', 1, 1, '2020-04-03 13:22:44', '2020-04-03 13:22:44', 0, 0, 0),
(409, 0, 'MENUPUBLIC_HOME', 'PUBLIC_HOME', NULL, 'Public Home', 1, 'MENU', 'Menu Master', '', 8, 'EN', 1, 1, '2020-05-21 13:22:44', '2020-05-21 13:22:44', 0, 0, 0);
