<?php

namespace App\Common;

use Illuminate\Support\Facades\App;
use App\email\Email;
use Illuminate\Http\Request;
use App\Model\MasterModel;
use App\Model\Menus;
use App\Model\MenusMapping;
use App\Model\RoleModel;
use Carbon\Carbon;
use Exception;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Lang;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Str;

class Common
{

    /* kalyani 27-dec-2021
       error_logging function moved to error_log module.
       this function used to error_log module.
    */

    // public function error_logging($e = array(), $function, $file)
    // {
    //     try {
    //         $userDetail = Auth::user();

    //         $userID = 0;
    //         $userName = '';
    //         if (!empty($userDetail)) {
    //             $userID = $userDetail->id;
    //             $userName = $userDetail->user_name;
    //         }

    //         $masterModel               = new MasterModel();
    //         $errorLogging['date']         = Carbon::now();
    //         $errorLogging['time']         = date('H:i:s');
    //         $errorLogging['user_id']      = $userID;
    //         $errorLogging['user_name']    = $userName;
    //         $errorLogging['page']         = $file;
    //         $errorLogging['function']     = $function;
    //         $errorLogging['error_code']   = $e->getCode();
    //         $errorLogging['description']  = $e->getMessage() . ' - ' . $e->getLine();

    //         // echo "<pre>";print_r($errorLogging);die;
    //         $rows_affected = $masterModel->insertData($errorLogging, 'error_logs');
    //         $errorString = "Error occur for user " . $userName . ", having user id " . $userID . " on  " . $file . "<br/> Following is error Message ." . $errorLogging['description'] . " having error code " . $errorLogging['error_code'];

    //         //$email = new Email();
    //         //$email->content_email(array('errors.sharedmachine@gmail.com'), 'Error Messages', 'Error Message', '', $errorString);

    //         return;
    //     } catch (Exception $ex) {
    //         $common     = new Common();
    //         $common->error_logging($ex, 'error_logging', 'Common.php');
    //         return view('layouts.coming_soon');
    //     }
    // }

    public function approve($to, $name, $subject, $attachment, $content)
    {
        try {
            $email = new Email();
            $email->content_email($to, $name, $subject, $attachment, $content);

            return;
        } catch (Exception $ex) {
             // $ErrorlogBaseController = new ErrorlogBaseController();
            // $ErrorlogBaseController->error_logging($ex,'approve', 'Common.php');
            //   return view('layouts.coming_soon');
        }
    }
    /*
     @ This function used for returning super admin menus by dynamically
     */
    static function superAdminMenus()
    {
        try {
            $obj = new Common();
            return $obj->getMenuDetails();
        } catch (Exception $ex) {
            // $ErrorlogBaseController = new ErrorlogBaseController();
            // $ErrorlogBaseController->error_logging($ex,'superAdminMenus', 'Common.php');
            //   return view('layouts.coming_soon');
        }
    }

    /*
     @ This function used for returning collab user menus by dynamically
     */
    static function collabUsersMenus()
    {
        try {
            $obj = new Common();
            return $obj->getMenuDetails();
        } catch (Exception $ex) {
             // $ErrorlogBaseController = new ErrorlogBaseController();
            // $ErrorlogBaseController->error_logging($ex,'collabUsersMenus', 'Common.php');
            //   return view('layouts.coming_soon');
        }
    }
    /*
     @ This function used for returning menus by dynamically
     */
    static function usersMenus()
    {
        try {
            $obj = new Common();
            return $obj->getMenuDetails();
        } catch (Exception $ex) {
              // $ErrorlogBaseController = new ErrorlogBaseController();
            // $ErrorlogBaseController->error_logging($ex,'collabUsersMenus', 'Common.php');
            //   return view('layouts.coming_soon');
        }
    }

    /*
    @ This function used for returning menus by dynamically
    */
    static function collabTeamMenus()
    {
        try {
            $obj = new Common();
            return $obj->getMenuDetails();
        } catch (Exception $ex) {
              // $ErrorlogBaseController = new ErrorlogBaseController();
            // $ErrorlogBaseController->error_logging($ex,'collabUsersMenus', 'Common.php');
            //   return view(layouts.coming_soon');
        }
    }


    /*
     @ Return drop down list options on code
     */

    static function dropDownList($code, $parent, $extraSettings = [], $locale = 'EN')
    {
        try {
            $query = DB::table('general_master')->select('code as value', 'description as label', 'parent_id');
            $query->where('master_code', '=', $code)->where('is_deleted', '=', '0');
            $query->where('locale_code', $locale);

            if ($parent) {
                $query->whereIn('parent_id', explode(",", $parent));
            }
            $query->orderBy('sequence', 'ASC');
            $data = json_decode(json_encode($query->get()), true);

            if (empty($data)) {
                $tableName = null;
                if ($code == 'PROSPECT-COMP') {
                    $tableName      = 'company';
                    $select         = ['id as value', 'company_name as label', DB::raw('null as parent_id')];
                    $extraSettings['where'] = [
                        'where' => [
                            ['column' => 'is_lead', 'expression' => '=', 'value' => 0],
                            ['column' => 'company.id', 'expression' => '>', 'value' => 1000]
                        ],
                    ];
                    $orderColumn    = 'company_name';
                    $orderBy        = 'ASC';
                } else if ($code == 'COMP') {
                    $tableName      = 'company';
                    $select         = ['id as value', 'company_name as label', DB::raw('null as parent_id')];
                    $extraSettings['where'] = [
                        'where' => [
                            ['column' => 'is_lead', 'expression' => '=', 'value' => 1],
                            ['column' => 'company.id', 'expression' => '>', 'value' => 1000]
                        ],
                    ];
                    $orderColumn    = 'company_name';
                    $orderBy        = 'ASC';
                } else if ($code == 'COMP_PROJECT') {
                    $tableName      = 'company_project';
                    $select         = ['id as value', 'project_name as label', DB::raw('null as parent_id')];
                    $extraSettings['where'] = [
                        'where' => [
                            ['column' => 'company_project.company_id', 'expression' => '=', 'value' => $parent]
                        ],
                    ];
                    $orderColumn    = 'project_name';
                    $orderBy        = 'ASC';
                } else if ($code == 'MOBMST') {
                    $tableName      = 'company_person';
                    $select         = ['id as value', 'mobile as label', DB::raw('null as parent_id')];
                    $where          = 'is_lead';
                    $orderColumn    = 'id';
                    $orderBy        = 'ASC';
                } else if ($code == 'EMILMST') {
                    $tableName      = 'company_person';
                    $select         = ['id as value', 'email as label', DB::raw('null as parent_id')];
                    $where          = 'is_lead';
                    $orderColumn    = 'id';
                    $orderBy        = 'ASC';
                } else if ($code == 'COMP_ADDR_CITY') {
                    # U S E  I N : Prospect - Overview/Customer Address Accordion  #SELECT id, CONCAT(company_name," - ",IFNULL(company_address_type,"")," / ",IFNULL(city_description,IFNULL(state_description,""))) AS company_name FROM
                    # Format company_name - company_address_type/city_description/state_description
                    $tableName      = 'company';
                    $select         = ['id as value', DB::raw('CONCAT(company_name," - ",IFNULL(company_address_description,"")," / ",IFNULL(city_description,IFNULL(state_description,""))) as label'), DB::raw('null as parent_id')];
                    $orderColumn    = 'company_name';
                    $orderBy        = 'ASC';
                } elseif ($code == 'COMP_PER') {
                    # use in connect for connect_to drop down field.
                    # here we want in format : company_name - first_name - last_name - mobile_no
                    $tableName      = 'company_person';
                    $select         = ['company_person.id as value', DB::raw('CONCAT(company.company_name," - ",company_person.first_name," ",company_person.last_name," - ",company_person.mobile) as label'), DB::raw('null as parent_id')];
                    $orderColumn    = 'company_person.first_name';
                    $distinct       = 'label';
                    $orderBy        = 'ASC';
                    $joinTableName  = 'company';
                    $firstOperator  = 'company.id';
                    $secondOperator = 'company_person.company_id';
                    if (isset($parent) && !empty($parent) && $parent != null && $parent != '') {
                        $extraSettings['where'] = [
                            'where' => [
                                ['column' => 'company_person.company_id', 'expression' => '=', 'value' => $parent]
                            ],
                        ];
                    }
                } elseif ($code == 'CONNECT_TRA') {
                    $tableName      = $extraSettings['table_name'];
                    $select         = ['company_person.id as value', 'company.id as company_id', DB::raw('CONCAT_WS(" ",company.company_name,"-",company_person.first_name,company_person.last_name,"-",company_person.mobile) as label')];

                    $query = DB::table($tableName)
                        ->select($select)
                        ->join('company_person', 'company_person.id', '=',  $tableName . '.contact_person_id')
                        ->join('company', 'company.id', '=', 'company_person.company_id');
                    $column = 'transaction_id';
                    if (self::isValidMd5($parent)) {
                        $column = DB::raw('md5(transaction_id)');
                    }
                    $query->where($column, $parent)->orderBy('label');
                    $data = json_decode(json_encode($query->get()), true);
                    return $data;
                } elseif ($code == 'MASTERTYPE') {
                    $tableName      = 'master_type';
                    $select         = ['code as value', DB::raw('CONCAT_WS("- ",description,code) as label'), DB::raw('null as parent_id')];
                    $orderColumn    = 'description';
                    $orderBy        = 'ASC';
                } elseif ($code == 'LEA_TYP') {
                    $tableName      = 'users';
                    $select         = ['id as value', 'name as label', DB::raw('null as parent_id')];
                    $orderColumn    = 'name';
                    $orderBy        = 'ASC';
                } elseif ($code == 'DAILY_MAC_CAT') {
                    $tableName      = 'machine_category_master';
                    $select         = ['id as value', 'name as label', DB::raw('null as parent_id')];
                    $extraSettings['where'] = [
                        'where' => [
                            ['column' => 'daily_rental', 'expression' => '=', 'value' => 1]
                        ],
                    ];

                    $orderColumn    = 'name';
                    $orderBy        = 'ASC';
                } elseif ($code == 'MAC_CAT') {
                    $tableName      = 'machine_category_master';
                    $select         = ['id as value', 'name as label', DB::raw('null as parent_id')];
                    $where          = 'id';
                    $orderColumn    = 'name';
                    $orderBy        = 'ASC';
                } elseif ($code == 'TRUCK_MODULE') {
                    $tableName      = 'machine_category_master';
                    $select         = ['id as value', 'name as label', DB::raw('null as parent_id')];
                    $orderColumn    = 'id';
                    $extraSettings['where'] = [
                        'where' => [
                            ['column' => 'truck_module', 'expression' => '=', 'value' => 'Y']
                        ],
                    ];
                    $orderBy        = 'ASC';
                }
                /*
                @ Only use when machine category change & return there capacity , make list details.
                */ elseif ($code == 'MAC_CAP') {
                    $tableName      = 'category_capacity_relation';
                    $select         = ['capacity_master.id as value', 'capacity_master.capacity as label', DB::raw('null as parent_id')];
                    $joinTableName  = 'capacity_master';
                    $firstOperator  = 'capacity_master.id';
                    $secondOperator = 'category_capacity_relation.capacities';
                    $where          = 'catg_id';
                    $orderColumn    = 'capacity';
                    $orderBy        = 'ASC';
                }
                /*
               @ Only use when machine category change & return there capacity , make list details.
               */ elseif ($code == 'MAC_ATTACH') {
                    $tableName      = 'category_attachment_relation';
                    $select         = ['attachment_master.id as value', 'attachment_master.machine_attachment as label', DB::raw('null as parent_id')];
                    $joinTableName  = 'attachment_master';
                    $firstOperator  = 'attachment_master.id';
                    $secondOperator = 'category_attachment_relation.attachment_id';
                    $where          = 'cat_id';
                    $orderColumn    = 'machine_attachment';
                    $orderBy        = 'ASC';
                } elseif ($code == 'MAC_MAKE') {
                    $tableName      = 'category_make_relation';
                    $select         = ['machine_make_master.id as value', 'machine_make_master.make_name as label', DB::raw('null as parent_id')];
                    $joinTableName  = 'machine_make_master';
                    $firstOperator  = 'machine_make_master.id';
                    $secondOperator = 'category_make_relation.makes';
                    $where          = 'category_make_relation.cat_id';
                    $orderColumn    = 'machine_make_master.make_name';
                    $orderBy        = 'ASC';
                } elseif ($code == 'ACT_OWN_LIS') {
                    # use in connect for Action Owner drop down field. - collab team members only
                    $tableName      = 'company_person';
                    $select         = ['id as value', DB::raw('CONCAT(company_person.first_name," ",company_person.last_name," - ",company_person.mobile) as label'), DB::raw('null as parent_id')];
                    $orderColumn    = 'first_name';
                    $orderBy        = 'ASC';
                } elseif ($code == 'MC_MAS') {
                    # use in supply for M/C Model drop down field.
                    $tableName      = 'machine_master';
                    $select         = ['id as value', 'machine_model as label', DB::raw('null as parent_id')];
                    $orderColumn    = 'machine_model';
                    $orderBy        = 'ASC';
                } elseif ($code == 'MAC_CAPACITY') {
                    # use in MachineCategoryMaster - Category - Category Accordion.
                    $tableName      = 'capacity_master';
                    $select         = ['id as value', DB::raw('CONCAT(capacity," - ",type) as label'), DB::raw('null as parent_id')];
                    $orderColumn    = 'capacity';
                    $orderBy        = 'ASC';
                } elseif ($code == 'MAC_ATTCH') {
                    # use in MachineCategoryMaster - Category - Attachment Accordion.
                    $tableName      = 'attachment_master';
                    $select         = ['id as value', 'machine_attachment as label', DB::raw('null as parent_id')];
                    $orderColumn    = 'machine_attachment';
                    $orderBy        = 'ASC';
                } elseif ($code == 'MAC_MAK') {
                    # use in MachineCategoryMaster - Category - Make Accordion.
                    $tableName      = 'machine_make_master';
                    $select         = ['id as value', 'make_name as label', DB::raw('null as parent_id')];
                    $orderColumn    = 'make_name';
                    $orderBy        = 'ASC';
                } elseif ($code == 'GRP_MST') {
                    # use in MachineCategoryMaster - Category - Make Accordion.
                    $tableName      = 'compare_group_master';
                    $select         = ['id as value', 'name as label', DB::raw('null as parent_id')];
                    $orderColumn    = 'name';
                    $orderBy        = 'ASC';
                } elseif ($code == 'PAR_MST') {
                    # use in MachineCategoryMaster - Parameter - Make Accordion.
                    $tableName      = 'compare_parameter_master';
                    $select         = ['id as value', 'name as label', DB::raw('null as parent_id')];
                    $orderColumn    = 'name';
                    $orderBy        = 'ASC';
                } elseif ($code == 'UNIT_MAS') {
                    # use in MachineCategoryMaster - Unit - Make Accordion.
                    $tableName      = 'units_master';
                    $select         = ['id as value', 'unit as label', DB::raw('null as parent_id')];
                    $orderColumn    = 'unit';
                    $orderBy        = 'ASC';
                } elseif ($code == 'COMPLIANCE_MAS') {
                    $tableName      = 'compliance_master';
                    $select         = ['id as value', 'name as label', DB::raw('null as parent_id')];
                    $orderColumn    = 'id';
                    $orderBy        = 'ASC';
                } elseif ($code == 'COMP_AUTH_MAS') {
                    $tableName      = 'compliance_authority';
                    $select         = ['id as value', 'authority_name as label', DB::raw('null as parent_id')];
                    $orderColumn    = 'id';
                    $orderBy        = 'ASC';
                }
                /*
               @ Milestone Add-Edit Form.  add_milestone.blade.php
               */ elseif ($code == 'FIN_CAT') {
                    $tableName      = 'finance_category';
                    $select         = ['id as value', 'name as label', DB::raw('null as parent_id')];
                    $orderColumn    = 'id';
                    $orderBy        = 'ASC';
                } elseif ($code == 'COLLAB_TEAM_MEM') {
                    $tableName      = 'users';
                    $select         = ['id as value', 'name as label', DB::raw('null as parent_id')];
                    $orderColumn    = 'name';
                    $where          = 'company_id';
                    $parent         = '5';               // id of collab team is 5.
                    $orderBy        = 'ASC';
                } elseif ($code == 'BIL_HEA') {
                    # Billing Head on the Milestone Form
                    $tableName      = 'finance_sub_category';
                    $select         = ['id as value', 'billing_head as label', DB::raw('null as parent_id')];
                    $orderColumn    = 'name';
                    $orderBy        = 'ASC';
                } elseif ($code == 'FIN_SUB_CAT') {
                    # Billing Head on the Milestone Form
                    $tableName      = 'finance_sub_category';
                    $select         = ['id as value',   DB::raw('CONCAT(name," - ",billing_head) as label'), DB::raw('null as parent_id')];
                    $orderColumn    = 'name';
                    $orderBy        = 'ASC';
                } elseif ($code == 'FIN_REF') {
                    # Milestone form filter for reference number
                    $tableName      = 'finance_milestone_details';
                    $select         = ['reference_number as value', 'reference_number as label', DB::raw('null as parent_id')];
                    $orderColumn    = 'reference_number';
                    $orderBy        = 'ASC';
                } elseif ($code == 'FIN_CON_REC') {
                    # Return Confirm milestone Receivable List
                    $tableName      = 'finance_milestone_details';
                    $select         = ['company.id as value', 'company.company_name as label', DB::raw('null as parent_id')];
                    $query = DB::table($tableName)
                        ->select($select)
                        ->leftJoin('finance_receivable', 'finance_receivable.milestone_id', '=', 'finance_milestone_details.id')
                        ->leftJoin('company', 'company.id', '=', 'finance_receivable.from_company_id')
                        ->where('milestone_status_code', 'CON')
                        ->groupBy('company.id')
                        ->orderBy('label');
                    $data = json_decode(json_encode($query->get()), true);
                    return $data;
                }

                if ($code == 'COMP-FULL') {
                    $tableName      = 'company';
                    $select         = [
                        'company.id as value',
                        DB::raw('CONCAT(company.company_name," - ", company_person.salutation," ",company_person.first_name," ",company_person.last_name,
                        " - ", company_person.mobile, " - ", company_person.email) as label'),
                        DB::raw('null as parent_id')
                    ];
                    $where          = 'company.is_lead';
                    $joinTableName  = 'company_person';
                    $firstOperator  = 'company_person.company_id';
                    $secondOperator = 'company.id';
                    $orderColumn    = 'company.company_name';
                    $orderBy        = 'ASC';
                } elseif ($code == 'COMP-PRO-CLS') {
                    #format  Sanjay Agarwal - Government
                    # description : Project Add - Edit.
                    $tableName      = 'company';
                    $select         = [
                        'company.id as value',
                        DB::raw('CONCAT_WS(" - ",company.company_name, general_master.description) as label'), DB::raw('null as parent_id')
                    ];
                    $where          = 'general_master.master_code';
                    $parent         = 'PRO_CLS';
                    $joinTableName  = 'general_master';
                    $firstOperator  = 'company.prospect_classification_code';
                    $secondOperator = 'general_master.code';
                    $orderColumn    = 'company.company_name';
                    $orderBy        = 'ASC';
                } else if ($code == 'COMP-ALL') {
                    $tableName      = 'company';
                    $select         = ['id as value', 'company_name as label', DB::raw('null as parent_id')];
                    $orderColumn    = 'company_name';
                    $orderBy        = 'ASC';
                    $extraSettings['where'] = [
                        'where' => [
                            ['column' => 'company.id', 'expression' => '>', 'value' => '0']
                        ],
                    ];
                } else if ($code == 'PRO_PRI_PER') {
                    # PROJECT PRIMARY PERSON. REQURED PROJECT ID AS INPUT
                    $tableName      = 'company_project_key_contact';
                    $select         = [
                        'company_project_key_contact.contact_person_id  as value',
                        DB::raw('CONCAT_WS(" ",company_person.salutation,company_person.first_name,company_person.last_name) AS label'),
                        DB::raw('null as parent_id')
                    ];

                    $query = DB::table('company_project_key_contact')
                        ->select($select)
                        ->join('company_person', 'company_project_key_contact.contact_person_id', '=', 'company_person.id')
                        ->where('company_project_key_contact.is_primary', '=', '1')
                        ->where('company_project_key_contact.project_id', '=', $parent)
                        ->groupBy('company_project_key_contact.contact_person_id')
                        ->orderBy('label');
                    $data = json_decode(json_encode($query->get()), true);
                    return $data;
                } elseif ($code == 'ROL_MAS') {
                    # Customer User form - Customer - Accordion / User tab
                    $tableName      = 'roles';
                    $select         = ['id as value', 'role as label', DB::raw('null as parent_id')];
                    $orderColumn    = 'role';
                    $where          = 'company_id';
                    $orderBy        = 'ASC';
                } elseif ($code == 'USER_MST') {
                    $tableName      = 'users';
                    $select         = ['id as value', 'name as label', DB::raw('null as parent_id')];
                    $orderColumn    = 'id';
                    $extraSettings['where'] = [
                        'where' => [
                            ['column' => 'users.company_id', 'expression' => '>', 'value' => 999]
                        ],
                    ];
                    $orderBy        = 'ASC';
                } elseif ($code == "COMP_USER") {
                    $tableName      = 'users';
                    $select         = [
                        'users.id  as value', DB::raw('CONCAT_WS(" ",users.name," - ",users.email," - ",users.mobile_no) AS label'),
                        DB::raw('null as parent_id')
                    ];
                    $query = DB::table($tableName)
                        ->select($select)
                        ->where('users.company_id', '=', $parent)
                        ->orderBy('users.id');
                    $data = json_decode(json_encode($query->get()), true);
                    return $data;
                }

                //Company Roles
                elseif ($code == 'COMP_ROLES') {
                    # Customer Dashboard -> Company Setup -> Users -> Add -> form
                    $tableName      = 'roles';
                    $select         = ['id as value', 'role as label', DB::raw('null as parent_id')];
                    $orderColumn    = 'role';
                    $extraSettings['where'] = [
                        'where' => [
                            ['column' => 'company_id', 'expression' => '=', 'value' => $parent]
                        ],
                    ];
                    $orderBy        = 'ASC';
                }
                /*
                * Operator Module Dropdown
                */ elseif ($code == "COMP_OPERATOR") {
                    $tableName      = 'operator';
                    $select         = [
                        'operator.id  as value',
                        DB::raw('CONCAT_WS(" ",operator.salutation,operator.first_name,operator.last_name) AS label'), DB::raw('null as parent_id')
                    ];
                    $query = DB::table($tableName)
                        ->select($select)
                        ->join('users', 'users.id', '=', 'operator.user_id')
                        ->where('operator.user_id', '>', '0')
                        ->where('users.id', '>', '1000')
                        ->where('users.company_id', '=', $parent)
                        ->orderBy('operator.id');
                    $data = json_decode(json_encode($query->get()), true);
                    return $data;
                }
                /*
                * Company Superviser list
                */ elseif ($code == "COMP_SUPE") {

                    $roleData = RoleModel::where('company_id', $parent)->where('role', 'Supervisor')->first();

                    $tableName      = 'users';
                    $select         = ['id as value', 'name as label', DB::raw('null as parent_id')];
                    $orderColumn    = 'id';
                    $extraSettings['where'] = [
                        'where' => [
                            ['column' => 'users.company_id', 'expression' => '=', 'value' => $parent], //10647
                            ['column' => 'users.role_id', 'expression' => '=', 'value' => $roleData->id] //108
                        ],
                    ];
                    $orderBy        = 'ASC';
                } elseif ($code == "USER_OPERATOR") {
                    $tableName      = 'operator';
                    $select         = [
                        'user_id as value', DB::raw('CONCAT_WS(" ",operator.salutation,operator.first_name,operator.last_name," - ",email," - ",mobile) AS label'), DB::raw('null as parent_id')
                    ];
                    $orderColumn    = 'operator.salutation';
                    $orderBy        = 'ASC';
                } elseif ($code == "OPERATOR_LIST") {
                    $tableName      = 'operator';
                    $select         = [
                        'id as value', DB::raw('CONCAT_WS(" ",operator.salutation,operator.first_name,operator.last_name," - ",email," - ",mobile) AS label'), DB::raw('null as parent_id')
                    ];
                    $orderColumn    = 'operator.salutation';
                    $orderBy        = 'ASC';
                } elseif ($code == "BILL_LIST") {
                    $tableName      = 'finance_bill';
                    $select         = [
                        'finance_bill.id as value', DB::raw('CONCAT_WS(" ",(CASE
                                        WHEN finance_bill.bill_type = "PI" THEN pi_bill_number
                                        WHEN finance_bill.bill_type = "TI" THEN  ti_bill_number
                                        END), " - ", company.company_name) AS label'), DB::raw('null as parent_id')
                    ];
                    $orderColumn    = 'label';
                    $joinTableName  = 'company';
                    $firstOperator  = 'company.id';
                    $secondOperator = 'finance_bill.company_id';
                    $orderBy        = 'ASC';
                } elseif ($code == "RECEIPT_LIST") {
                    // GET LIST OF RECEIPT
                    $tableName      = 'finance_receipt';
                    $select         = ['finance_receipt.id as value', 'finance_receipt.id AS label', DB::raw('null as parent_id')];
                    $orderColumn    = 'label';
                    $orderBy        = 'ASC';
                } elseif ($code == 'INVOICE_LIST') {
                    // GET LIST OF Invoice
                    $tableName      = 'finance_invoice';
                    $select         = [
                        'finance_invoice.id as value', DB::raw('CONCAT_WS(" ",(CASE
                                        WHEN finance_invoice.invoice_type = "PI" THEN pi_invoice_number
                                        WHEN finance_invoice.invoice_type = "TI" THEN  ti_invoice_number
                                        END), " - ", company.company_name) AS label'), DB::raw('null as parent_id')
                    ];
                    $orderColumn    = 'label';
                    $joinTableName  = 'company';
                    $firstOperator  = 'company.id';
                    $secondOperator = 'finance_invoice.company_id';
                    $orderBy        = 'ASC';
                }

                /* USe For API Code*/ elseif ($code == 'MAC_CAT_ATTACH') {
                    $tableName      = 'category_attachment_relation';
                    $select         = ['cat_id as cat_id', 'attachment_id as attachment_id', DB::raw('null as parent_id')];
                    $orderColumn    = 'cat_id';
                    $orderBy        = 'ASC';
                } elseif ($code == 'MAC_CAT_CAP') {
                    $tableName      = 'category_capacity_relation';
                    $select         = ['catg_id as cat_id', 'capacities as capacitie_id', DB::raw('null as parent_id')];
                    $orderColumn    = 'cat_id';
                    $orderBy        = 'ASC';
                } elseif ($code == 'MAC_CAT_MAKE') {
                    $tableName      = 'category_make_relation';
                    $select         = ['cat_id as cat_id', 'makes as make_id', DB::raw('null as parent_id')];
                    $orderColumn    = 'cat_id';
                    $orderBy        = 'ASC';
                } elseif ($code == 'REN_MAP_COMP') {
                    // On Rental Requirement Offer screen. List of Companies that are mapped. $parent = requirement id
                    $query = DB::table('company')
                        ->select('company.id as value', 'company.company_name as label')
                        ->join('rental_requirement_mapping', 'rental_requirement_mapping.company_id', '=', 'company.id')
                        ->where(DB::raw('md5(rental_requirement_mapping.requirement_id)'), $parent)
                        ->groupBy('company.id')
                        ->orderBy('company.company_name');
                    $data = json_decode(json_encode($query->get()), true);
                    return $data;
                } elseif ($code == 'RES_MAP_COMP') {
                    // On Resale Requirement Offer screen. List of Companies that are mapped. $parent = requirement id
                    $query = DB::table('company')
                        ->select('company.id as value', 'company.company_name as label')
                        ->join('used_requirement_mapping', 'used_requirement_mapping.company_id', '=', 'company.id')
                        ->where(DB::raw('md5(used_requirement_mapping.requirement_id)'), $parent)
                        ->groupBy('company.id')
                        ->orderBy('company.company_name');
                    $data = json_decode(json_encode($query->get()), true);
                    return $data;
                } elseif ($code == 'DLY_REN_MAP_COMP') {
                    // On Daily Rental Requirement Offer screen. List of Companies that are mapped. $parent = requirement id
                    $query = DB::table('company')
                        ->select('company.id as value', 'company.company_name as label')
                        ->join('daily_rental_requirement_mapping', 'daily_rental_requirement_mapping.company_id', '=', 'company.id')
                        ->where(DB::raw('md5(daily_rental_requirement_mapping.requirement_id)'), $parent)
                        ->groupBy('company.id')
                        ->orderBy('company.company_name');
                    $data = json_decode(json_encode($query->get()), true);
                    return $data;
                }
                if (isset($tableName) && !empty($tableName)) {
                    $query = DB::table($tableName)->select($select);

                    if (isset($joinTableName) && !empty($joinTableName)) {
                        $query->leftJoin($joinTableName, $firstOperator, '=', $secondOperator);
                    }
                    if (isset($where) && !empty($where) && !empty($parent)) {
                        $query->where($where, '=', $parent)->where($tableName . '.' . 'is_deleted', '=', 0);
                    } else if (!empty($extraSettings['where']) && count($extraSettings['where']) > 0) {

                        $query = MasterModel::queryBinder($extraSettings, $query);
                    }

                    $query->orderBy($orderColumn, $orderBy);

                    $data = json_decode(json_encode($query->get()), true);
                } else {
                    $data = "";
                }
            }

            return $data;
        } catch (Exception $ex) {
               // $ErrorlogBaseController = new ErrorlogBaseController();
            // $ErrorlogBaseController->error_logging($ex,'dropDownList', 'Common.php');
            //   return view('errors.oh!');
            print_r($ex->getLine());
            print_r($ex->getMessage());

        }
    }

    public static function getMenuDetails()
    {
        $obj = new Common();
        try {
            $sessionData    = session()->get('user_info');
            // $currentPath    = Route::getFacadeRoot()->current()->uri();
            $currentPath    =  $sessionData['redirect_url'];
            $menuDetailByURL = MenusMapping::where('url', '=', $currentPath)->where('menu_mapping.is_deleted', '=', '0')->first(); // id =30 , menu_master_id=7 , $current_path=collab_team
            $string         = '';
            $parentMenuList = MenusMapping::select('menu_mapping.*', 'menus.description', 'menus.language_key')
                ->leftJoin('menus', 'menu_mapping.menu_id', '=', 'menus.id')
                ->where('menu_master_id', '=', $menuDetailByURL->menu_master_id)
                ->where('parent_id', '=', 0)->where('menu_mapping.is_deleted', '=', '0')
                ->orderBy('sequence', 'ASC')
                ->get();

            if (!$parentMenuList->isEmpty()) {
                foreach ($parentMenuList as $parentMenuDetail) {

                    $childMenuList = MenusMapping::select('menu_mapping.*', 'menus.description', 'menus.language_key')
                        ->leftJoin('menus', 'menu_mapping.menu_id', '=', 'menus.id')
                        ->where('parent_id', '=', $parentMenuDetail->id)->where('menu_mapping.is_deleted', '=', '0')
                        ->orderBy('sequence', 'ASC')
                        ->get();


                    if (!$childMenuList->isEmpty()) {

                        $string .= '<li class="nav-item dropdown br-right">
                                    <a class="navbar-link-color" id="navbarDropdown" href="' . url('/' . $parentMenuDetail->url) . '" role="button" data-toggle="dropdown" data-menu="' . $obj->get_translation($parentMenuDetail->language_key, $parentMenuDetail->description) . '">
                                       ' . $obj->get_translation($parentMenuDetail->language_key, $parentMenuDetail->description) . '<span class="caret"></span>
                                    </a>
                                    <ul class="dropdown-menu multi-level">';

                        foreach ($childMenuList as $childMenuDetail) {

                            $subChildMenuList = MenusMapping::select('menu_mapping.*', 'menus.description', 'menus.language_key')
                                ->leftJoin('menus', 'menu_mapping.menu_id', '=', 'menus.id')
                                ->where('parent_id', '=', $childMenuDetail->id)->where('menu_mapping.is_deleted', '=', '0')
                                ->orderBy('sequence', 'ASC')
                                ->get();

                            $string .= '';

                            if (!$subChildMenuList->isEmpty()) {
                                foreach ($subChildMenuList as $subChildMenuDetail) {
                                    $string .= '<li class="dropdown-submenu"><a href="' . url('/' . $childMenuDetail->url) . '" class="dropdown-toggle level2" data-toggle="dropdown">' . $obj->get_translation($parentMenuDetail->language_key, $childMenuDetail->description) . '</a>
                                                <ul class="dropdown-menu">
                                                    <li class="333"><a href="' . url('/' . $subChildMenuDetail->url) . '" class="level3">' . $obj->get_translation($parentMenuDetail->language_key, $subChildMenuDetail->description) . '</a></li>
                                                </ul>
                                            </li>';
                                }
                            } else {
                                if ($childMenuDetail->url == '#') {
                                    $string .= '<a class="dropdown-item 222" data-subMenu="' . $obj->get_translation($parentMenuDetail->language_key, $childMenuDetail->description) . '" href="#">' . $obj->get_translation($parentMenuDetail->language_key, $childMenuDetail->description) . '</a>';
                                } else {
                                    $string .= '<a class="dropdown-item 222" data-subMenu="' . $obj->get_translation($parentMenuDetail->language_key, $childMenuDetail->description) . '" href=" ' . url('/' . $childMenuDetail->url) . ' ">' . $obj->get_translation($parentMenuDetail->language_key, $childMenuDetail->description) . '</a>';
                                }
                            }
                        }
                        $string .= '</ul></li>';
                    } else {
                        if ($parentMenuDetail->short_code != 'HOME') {
                            if ($parentMenuDetail->url == '#') {
                                $string .= '<li class="nav-item br-right"><a class="navbar-link-color" data-menu="' . $obj->get_translation($parentMenuDetail->language_key, $parentMenuDetail->description) . '" href="#" data-secondmenu="none">' . $obj->get_translation($parentMenuDetail->language_key, $parentMenuDetail->description) . '</a></li>';
                            } else {
                                $string .= '<li class="nav-item br-right"><a class="navbar-link-color" data-menu="' . $obj->get_translation($parentMenuDetail->language_key, $parentMenuDetail->description) . '" href="' . url('/' . $parentMenuDetail->url) . '" data-secondmenu="none">' . $obj->get_translation($parentMenuDetail->language_key, $parentMenuDetail->description) . '</a></li>';
                            }
                        }
                    }
                }
            }
            return $string;
        } catch (Exception $ex){
            // $ErrorlogBaseController = new ErrorlogBaseController();
            // $ErrorlogBaseController->error_logging($ex,'getMenuDetails', 'Common.php');
            //   return view('layouts.coming_soon');

        }
    }

    public static function getMenu($menuCode = 'USER_DASH')
    {
        try {
            $sessionData    = session()->get('user_info');
            $obj = new Common();
            $parent         = MenusMapping::select('menu_mapping.*', 'menus.description', 'menus.language_key')
                ->leftJoin('menus', 'menu_mapping.menu_id', '=', 'menus.id')
                ->where('menu_mapping.is_deleted', '=', '0')
                ->where('menu_mapping.parent_id', '=', 0)
                ->whereRaw("menu_mapping.menu_master_id = (select id from general_master where code = '$menuCode')")
                ->orderBy('sequence')
                ->get();

            $parentSub      = array();
            $parentMenu     = array();
            if (!$parent->isEmpty()) {
                foreach ($parent as $parentDetail) {
                    $child  = MenusMapping::select('menu_mapping.*', 'menus.description', 'menus.language_key')
                        ->leftJoin('menus', 'menu_mapping.menu_id', '=', 'menus.id')
                        ->where('parent_id', $parentDetail->id)->where('menu_mapping.is_deleted', '=', '0')
                        ->get();

                    if (!$child->isEmpty()) {
                        // Added by Shrikant. SMWE-107 Dashboard Menus Images into One Single Image
                        $styles = Menus::find($parentDetail->menu_id)->first();

                        $parentSub['parent']['url'] = $parentDetail->url;
                        $parentSub['parent']['id'] = $parentDetail->menu_id;
                        $parentSub['parent']['description'] = $obj->get_translation($parentDetail->language_key, $parentDetail->description);
                        $parentSub['parent']['font_icon_class'] = $parentDetail->font_icon_class;
                        // Added by Shrikant. SMWE-107 Dashboard Menus Images into One Single Image
                        $parentSub['parent']['styles']      = $styles->styles;
                        $parentMenu[$parentDetail->id]['parent_child'] = $parentSub;

                        foreach ($child as $childDetail) {
                            $styles = Menus::find($childDetail->menu_id)->first();
                            $parentMenu[$parentDetail->id]['parent_child']['child'][$childDetail->id]['menu_id'] = $childDetail->menu_id;
                            $parentMenu[$parentDetail->id]['parent_child']['child'][$childDetail->id]['menu_mapping_id'] = $childDetail->id;
                            $parentMenu[$parentDetail->id]['parent_child']['child'][$childDetail->id]['url'] = $childDetail->url;
                            $parentMenu[$parentDetail->id]['parent_child']['child'][$childDetail->id]['description'] = $obj->get_translation($childDetail->language_key, $childDetail->description);
                            $parentMenu[$parentDetail->id]['parent_child']['child'][$childDetail->id]['font_icon_class'] = $childDetail->font_icon_class;
                            $parentMenu[$parentDetail->id]['parent_child']['child'][$childDetail->id]['styles']      =  $styles->styles;
                        }
                    } else {
                        $styles = Menus::find($parentDetail->menu_id)->first();
                        $parentMenu[$parentDetail->id]['parent']['id'] = $parentDetail->menu_id;
                        $parentMenu[$parentDetail->id]['parent']['url'] = $parentDetail->url;
                        $parentMenu[$parentDetail->id]['parent']['description'] = $obj->get_translation($parentDetail->language_key, $parentDetail->description);
                        $parentMenu[$parentDetail->id]['parent']['font_icon_class'] = $parentDetail->font_icon_class;
                        $parentMenu[$parentDetail->id]['parent']['styles']      =  $styles->styles;
                    }
                }
            }
            return $parentMenu;
        } catch (Exception $ex) {
             // $ErrorlogBaseController = new ErrorlogBaseController();
            // $ErrorlogBaseController->error_logging($ex,'getMenu', 'Common.php');
            //   return view('layouts.coming_soon');
        }
    }

    public static function getCityNameByLatitudeLongitude($latlong)
    {
        try {
            $APIKEY = "AIzaSyBLo2emU_Wmv90fXMRdlAmoi500Mk0Ke-4"; // Replace this with your google maps api key
            $googleMapsUrl = "https://maps.googleapis.com/maps/api/geocode/json?latlng=" . $latlong . "&language=ar&key=" . $APIKEY;
            $response = file_get_contents($googleMapsUrl);
            $response = json_decode($response, true);
            $results = $response["results"];
            $addressComponents = $results[0]["address_components"];
            $cityName = "";
            foreach ($addressComponents as $component) {
                // echo $component;
                $types = $component["types"];
                if (in_array("locality", $types) && in_array("political", $types)) {
                    $city['long_name'] = $component["long_name"];
                    $city['short_name'] = $component["short_name"];
                }
            }
            if ($cityName == "") {
                echo "Failed to get CityName";
            } else {
                //echo $cityName;
                return $cityName;
            }
        } catch (Exception $ex) {
              // $ErrorlogBaseController = new ErrorlogBaseController();
            // $ErrorlogBaseController->error_logging($ex,'getCityNameByLatitudeLongitude', 'Common.php');
            //   return view('layouts.coming_soon');

        }
    }

    /*
     @ Return description on code form General Master.
     */

    public static function code_description($code)
    {
        try {
            $query = DB::table('general_master')->select('description')
                ->where('code', '=', $code);

            $data = json_decode(json_encode($query->first()), true);
            return $data['description'];
        } catch (Exception $ex) {
               // $ErrorlogBaseController = new ErrorlogBaseController();
            // $ErrorlogBaseController->error_logging($ex,'code_description', 'Common.php');
            //   return view('layouts.coming_soon');
        }
    }

    /*
     @ calculates the distance between two points (given the latitude/longitude of those points).
     */
    public static function distance($lat1, $lon1, $lat2, $lon2, $unit)
    {
        try {
            if (($lat1 == $lat2) && ($lon1 == $lon2)) {
                return 0;
            } else {
                $theta = $lon1 - $lon2;
                $dist = sin(deg2rad($lat1)) * sin(deg2rad($lat2)) +  cos(deg2rad($lat1)) * cos(deg2rad($lat2)) * cos(deg2rad($theta));
                $dist = acos($dist);
                $dist = rad2deg($dist);
                $miles = $dist * 60 * 1.1515;
                $unit = strtoupper($unit);

                if ($unit == "K") {
                    return ($miles * 1.609344);
                } else if ($unit == "N") {
                    return ($miles * 0.8684);
                } else {
                    return $miles;
                }
            }
        } catch (Exception $ex) {
               // $ErrorlogBaseController = new ErrorlogBaseController();
            // $ErrorlogBaseController->error_logging($ex,'code_description', 'Common.php');
            //   return view('layouts.coming_soon');
        }
    }

    /*
     * Return Save Filter
     */
    public static function saveFilter($query, $request, $sessionData, $FormID, $settings)
    {
        try {
            $requestData = $request->all();
            $masterModel = new MasterModel();


            if (isset($requestData['filterSubmit']) && !empty($requestData['filterSubmit'])) {
                $query = DB::table('forms_filter')->where('form_id', '=', $FormID)
                    ->where('user_id', $sessionData['user_id']);
                if (isset($requestData['service'])) {
                    $query = $query->where('service_type', $requestData['service']);
                }
                $filterData = $query->first();

                $oldFilterData  = json_decode(json_encode($filterData), true);
                $filterString   = json_encode($requestData);

                if (empty($oldFilterData)) {

                    $insertFilterData['form_id']        = $FormID;
                    $insertFilterData['filter_string']  = $filterString;
                    $insertFilterData['user_id']        = $sessionData['user_id'];
                    $insertFilterData['service_type']   = !empty($requestData['service']) ? $requestData['service'] : '';
                    $masterModel->insertData($insertFilterData, 'forms_filter');
                } else {

                    $updateFilterData['filter_string']  = $filterString;
                    $updateFilterData['user_id']        = $sessionData['user_id'];
                    $updateFilterData['service_type']   = !empty($requestData['service']) ? $requestData['service'] : '';

                    $where['form_id']       = $FormID;
                    $where['user_id']       = $sessionData['user_id'];
                    if (isset($requestData['service'])) {
                        $where['service_type'] = $requestData['service'];
                    }
                    $masterModel->updateData($updateFilterData, 'forms_filter', $where);
                }

                /*foreach ($filterFields as $filterKey => $filterVal) {
                $filter         = $filterFields[$filterKey]->name;
                $filterColumn   = $filterFields[$filterKey]->db_mapping;

                if (isset($requestData[$filter])) {

                    $query->whereIn($filterColumn, $requestData[$filter]);
                }
            }*/
            } else {

                $oldFilterData = DB::table('forms_filter')->where('form_id', $FormID)->where('user_id', $sessionData['user_id']);
                if (isset($requestData['service'])) {
                    $oldFilterData = $oldFilterData->where('service_type', $requestData['service']);
                }
                $oldFilterData = $oldFilterData->first();

                if (!empty($oldFilterData)) {
                    $filterOldData = json_decode($oldFilterData->filter_string);
                    $settings = $filterOldData;

                    foreach ($filterOldData as $filterKey => $filterVal) {

                        $fieldByFilterName       = DB::table('form_fields')->where('form_id', $FormID) //46
                            ->where('is_deleted', '=', '0')->where('name', $filterKey)
                            ->orderBy('sequence', 'asc')->first();

                        if (!empty($fieldByFilterName)) {
                            if ($fieldByFilterName->type == 'location') {
                                $query = $query->where($fieldByFilterName->db_mapping, '=', $filterVal);
                            } else if ($fieldByFilterName->type == 'date') {
                                $query = $query->whereBetween($fieldByFilterName->db_mapping, [$filterOldData->filter_from_date, $filterOldData->filter_to_date]);
                            } else if ($fieldByFilterName->type == 'month') {
                                $query = $query->whereBetween($fieldByFilterName->db_mapping, [$filterOldData->filter_from_date, $filterOldData->filter_to_date]);
                            } else if ($fieldByFilterName->type == 'text') {
                                $query = $query->where($fieldByFilterName->db_mapping, 'LIKE', '%' .  $filterVal . '%');
                            } else if ($filterKey == 'listed_for') {
                                // Use in dashboard machine list filter.
                                $query->where(function ($query) use ($filterVal) {
                                    if (in_array("REN", $filterVal)) {
                                        $query = $query->orWhere('machine_basic_detail.rental', 1);
                                    }
                                    if (in_array("RES", $filterVal)) {
                                        $query = $query->orWhere('machine_basic_detail.resale', 1);
                                    }
                                    if (in_array("DLY-REN", $filterVal)) {
                                        $query = $query->orWhere('machine_basic_detail.daily_rental', 1);
                                    }
                                });
                            } else {
                                $query->whereIn($fieldByFilterName->db_mapping, $filterVal);
                            }
                        }
                    }
                }
            }

            $no_of_filter_set = self::countNumberOfFilterSet($FormID, $sessionData,  $requestData);
            return ['query' => $query, 'settings' => $settings, 'no_of_filter_set' => $no_of_filter_set];
        } catch (Exception $ex) {
               // $ErrorlogBaseController = new ErrorlogBaseController();
            // $ErrorlogBaseController->error_logging($ex,'saveFilter', 'Common.php');
            //   return view('layouts.coming_soon');
        }
    }

    static function countNumberOfFilterSet($FormID, $sessionData,  $requestData)
    {
        try {
            $no_of_filter_set = 0;
            $oldFilterData = DB::table('forms_filter')->where('form_id', $FormID)->where('user_id', $sessionData['user_id']);
            if (isset($requestData['service'])) {
                $oldFilterData = $oldFilterData->where('service_type', $requestData['service']);
            }
            $oldFilterData = $oldFilterData->first();

            if (!empty($oldFilterData)) {
                $filterOldData = json_decode($oldFilterData->filter_string);

                foreach ($filterOldData as $filterKey => $filterVal) {
                    $fieldByFilterName       = DB::table('form_fields')->where('form_id', $FormID)
                        ->where('is_deleted', '=', '0')->where('name', $filterKey)
                        ->orderBy('sequence', 'asc')->first();

                    if (!empty($fieldByFilterName)) {
                        $no_of_filter_set++;
                    }
                }
            }
            return $no_of_filter_set;
        } catch (Exception $ex) {
                   // $ErrorlogBaseController = new ErrorlogBaseController();
            // $ErrorlogBaseController->error_logging($ex,'countNumberOfFilterSet', 'Common.php');
            //   return view('layouts.coming_soon');
        }
    }
    public static function objectElements($object, $type, $ids = [])
    {
        try {
            $mainList = [];
            if (!empty($object)) {
                foreach ($object as $objItem) {
                    $query = DB::table(DB::raw($objItem->view));
                    if ($type == 'emails') {
                        $query->select(DB::raw($objItem->emails));
                    } elseif ($type == 'ids' || $type == 'to_ids') {
                        $query->select(DB::raw($objItem->ids));
                    } elseif ($type == 'mobile_numbers') {
                        $query->select(DB::raw($objItem->mobiles));
                    } else {
                        // Variables on editor
                        $query->select('*');
                    }
                    if (!empty($objItem->ids)) {
                        $where = explode(',', $objItem->ids);
                        if (!empty($ids) && array_key_exists($where[0], $ids)) {
                            $query->where(DB::raw($where[0]), $ids[$where[0]]);
                        }
                    }
                    $list = $query->first();

                    if (!empty($list)) {
                        foreach ($list as $key => $detail) {
                            $objectsItems = [];
                            if (empty($ids)) {
                                $objectsItems['Type'] = 'Text';
                                $objectsItems['Code'] = '{' . ucwords(str_replace('_', ' ', $key)) . '}';
                                $objectsItems['Name'] = ucwords(str_replace('_', ' ', $key));
                            }
                            if ($type == 'emails' || $type == 'mobile_numbers') {
                                $mainList[] = $objectsItems;
                            } elseif ($type == 'ids') {
                                $objectsItems['Code'] = $key;
                                $mainList[] = $objectsItems;
                            } elseif ($type == 'to_ids') {
                                $mainList[] = $objectsItems;
                            } elseif ($type == 'parameters') {
                                $mainList['{' . ucwords(str_replace('_', ' ', $key)) . '}'] = $detail;
                            } else {
                                if ($key !== 'userid' || $key !== 'username') {
                                    $mainList[$objItem->name][] = $objectsItems;
                                }
                            }
                        }
                    }
                    /// Swapnil 19-Apr-21
                    //$collab_email = 'info@collab.com';
                    // $collab_email = 'hello@sharedmachine.in';

                    // if ($objItem == 'User') {
                    //     $query = DB::table('users')->leftJoin('login_with_otp', 'login_with_otp.user_id', '=', 'users.id')
                    //         ->orderBy('login_with_otp.created_at', 'desc');
                    //     if ($type == 'emails') {
                    //         $query->select('users.email AS user_email', DB::raw('"' . $collab_email . '" as collab_email'));
                    //     } elseif ($type == 'ids' || $type == 'to_ids') {
                    //         $query->select('users.id AS user_id');
                    //     } elseif ($type == 'mobile_numbers') {
                    //         $query->select('users.mobile_no AS user_mobile_no');
                    //     } else {
                    //         // Variables on editor
                    //         $query->select(
                    //             'users.id AS user_id',
                    //             'users.name AS user_name',
                    //             'users.email AS user_email',
                    //             'users.mobile_no AS user_mobile_no',
                    //             'firm_name AS user_firm_name',
                    //             DB::raw('"' . $collab_email . '" as collab_email'),
                    //             'otp as OTP'
                    //         );
                    //     }

                    //     if (!empty($ids) && array_key_exists("user_id", $ids)) {
                    //         $query->where('users.id', $ids['user_id']);
                    //     }
                    //     $list = $query->first();

                    //     foreach ($list as $key => $detail) {
                    //         $objectsItems = [];
                    //         if (empty($ids)) {
                    //             $objectsItems['Type'] = 'Text';
                    //             $objectsItems['Code'] = '{' . ucwords(str_replace('_', ' ', $key)) . '}';
                    //             $objectsItems['Name'] = ucwords(str_replace('_', ' ', $key));
                    //         }
                    //         if ($type == 'emails' || $type == 'mobile_numbers') {
                    //             $mainList[] = $objectsItems;
                    //         } elseif ($type == 'ids') {
                    //             $objectsItems['Code'] = $key;
                    //             $mainList[] = $objectsItems;
                    //         } elseif ($type == 'to_ids') {
                    //             $mainList[] = $objectsItems;
                    //         } elseif ($type == 'parameters') {
                    //             $mainList['{' . ucwords(str_replace('_', ' ', $key)) . '}'] = $detail;
                    //         } else {
                    //             $mainList[$objItem][] = $objectsItems;
                    //         }
                    //     }
                    // }

                    // if ($objItem == 'Company') {
                    //     $query = DB::table('company');
                    //     if ($type == 'emails') {
                    //         $query->select('id AS company_id');
                    //     } elseif ($type == 'ids' || $type == 'to_ids') {
                    //         $query->select('id AS company_id');
                    //     } elseif ($type == 'mobile_numbers') {
                    //         $query->select('id AS company_id');
                    //     } else {
                    //         $query->select('id AS company_id', 'company_name', 'company_address_type', 'company_address_description', 'city_description', 'state_description', 'gst_no');
                    //     }

                    //     if (!empty($ids) && array_key_exists("company_id", $ids)) {
                    //         $query->where('id', $ids['company_id']);
                    //     }
                    //     $list = $query->first();
                    //     foreach ($list as $key => $detail) {
                    //         $objectsItems = [];
                    //         if (empty($ids)) {
                    //             $objectsItems['Type'] = 'Text';
                    //             $objectsItems['Code'] = '{' . ucwords(str_replace('_', ' ', $key)) . '}';
                    //             $objectsItems['Name'] = ucwords(str_replace('_', ' ', $key));
                    //         }
                    //         if ($type == 'emails' || $type == 'mobile_numbers') { //emails, mobile_numbers,
                    //             $mainList[] = $objectsItems;
                    //         } elseif ($type == 'ids') {
                    //             $objectsItems['Code'] = $key;
                    //             $mainList[] = $objectsItems;
                    //         } elseif ($type == 'to_ids') {
                    //             $mainList[] = $objectsItems;
                    //         } elseif ($type == 'parameters') { //parameters //userID (notifications)
                    //             $mainList['{' . ucwords(str_replace('_', ' ', $key)) . '}'] = $detail;
                    //         } else {
                    //             $mainList[$objItem][] = $objectsItems;
                    //         }
                    //     }
                    // }

                    // if ($objItem == 'Company Workflow') {
                    //     $query = DB::table('company_workflow as cw');
                    //     if ($type == 'emails') {
                    //         $query->select('cw.approver_name AS company_workflow_approver_name');//,'cw.approver_name as company_workflow_approver_name');
                    //     // } elseif ($type == 'ids' || $type == 'to_ids') {
                    //     //     $query->select('cw.id AS company_id');
                    //     // } elseif ($type == 'mobile_numbers') {
                    //     //     $query->select('cw.id AS company_id');
                    //     // } else {
                    //     //     $query->select('company.id AS company_id', 'company_name', 'company_address_type', 'company_address_description', 'city_description', 'state_description', 'gst_no');
                    //     // }

                    //     // if (!empty($ids) && array_key_exists("company_id", $ids)) {
                    //     //     $query->where('company.id', $ids['company_id']);
                    //     // }
                    //     $list = $query->first();
                    //     foreach ($list as $key => $detail) {
                    //         $objectsItems = [];
                    //         if (empty($ids)) {
                    //             $objectsItems['Type'] = 'Text';
                    //             $objectsItems['Code'] = '{' . ucwords(str_replace('_', ' ', $key)) . '}';
                    //             $objectsItems['Name'] = ucwords(str_replace('_', ' ', $key));
                    //         }
                    //         if ($type == 'emails' || $type == 'mobile_numbers') { //emails, mobile_numbers,
                    //             $mainList[] = $objectsItems;
                    //         } elseif ($type == 'ids') {
                    //             $objectsItems['Code'] = $key;
                    //             $mainList[] = $objectsItems;
                    //         } elseif ($type == 'to_ids') {
                    //             $mainList[] = $objectsItems;
                    //         } elseif ($type == 'parameters') { //parameters //userID (notifications)
                    //             $mainList['{' . ucwords(str_replace('_', ' ', $key)) . '}'] = $detail;
                    //         } else {
                    //             $mainList[$objItem][] = $objectsItems;
                    //         }
                    //     }
                    // }
                }
            }
            return $mainList;
        } catch (Exception $ex){
               // $ErrorlogBaseController = new ErrorlogBaseController();
            // $ErrorlogBaseController->error_logging($ex,'objectElements', 'Common.php');
            //   return view('layouts.coming_soon');
        }
    }

    public static function sendAlert($event, $keys)
    {
        $language = 'HN';
        $masterModel = new MasterModel();

        try {
            //Email Listing
            $eventEmailList      = DB::table('event_master')
                ->join('event_email_master', 'event_master.id', '=', 'event_email_master.event_master_id')
                ->where('event_master.event_code', $event)
                ->orderBy('sequence', 'ASC')
                ->get();

            if (!empty($eventEmailList)) {
                foreach ($eventEmailList as $key => $eventDetail) {
                    $emailDetail    = DB::table('email_master')
                        ->join('email_master_elements', 'email_master.id', 'email_master_elements.email_master_id')
                        ->where('email_master.id', $eventDetail->email_master_id)
                        ->where('email_master_elements.language', $language)
                        ->first();

                    if (empty($emailDetail)) {
                        $emailDetail    = DB::table('email_master')
                            ->join('email_master_elements', 'email_master.id', 'email_master_elements.email_master_id')
                            ->where('email_master.id', $eventDetail->email_master_id)
                            ->where('email_master_elements.language', 'EN')
                            ->first();
                    }

                    if (!empty($emailDetail)) {
                        $associatedObjects  = [];
                        if (!empty($eventDetail)) {
                            $associatedObjects  = DB::table('object_master')->whereIn('id', explode(',', $emailDetail->associated_objects))->get();
                        }

                        $objectNames        = [];
                        foreach ($associatedObjects as $object) {
                            $objectNames[]  = $object->name;
                        }

                        $vars       = Common::objectElements($objectNames, 'parameters', $keys);
                        $to_emails  = strtr($emailDetail->to_email_id, $vars);
                        $cc_emails  = strtr($emailDetail->cc_email_id, $vars);
                        $subject    = strtr($emailDetail->subject, $vars);

                        $header     = strtr($emailDetail->mail_header, $vars);
                        $message    = strtr($emailDetail->mail_body, $vars);
                        $footer     = strtr($emailDetail->mail_footer, $vars);

                        // echo '<b>To emails</b> - ' . $to_emails . '<br>';
                        // echo '<b>CC emails</b> - ' . $cc_emails . '<br>';
                        // echo '<b>Subject</b> - ' . $subject . '<br>';
                        // echo '<b>Body</b> - <br>';
                        // echo '<div style="margin-left: 30px;">' . $header . '</div>';
                        // echo '<div style="margin-left: 30px;">' . $message . '</div>';
                        // echo '<div style="margin-left: 30px;">' . $footer . '</div>';
                        // echo '<br>----------email end------------<br><br>';

                        $bodyString = $header . '<br>' . $message . '<br>' . $footer;

                        $email = new Email();
                        //  $email->content_email([$to_emails], $subject, $subject, '', $bodyString);
                    }
                }
            }

            //SMS Listing
            $eventSMSList       = DB::table('event_master')
                ->join('event_sms_master', 'event_master.id', '=', 'event_sms_master.event_master_id')
                ->where('event_master.event_code', $event)
                ->get();

            if (!empty($eventSMSList)) {
                foreach ($eventSMSList as $key => $eventDetail) {

                    $smsDetail      = DB::table('sms_master')
                        ->join('sms_master_elements', 'sms_master.id', 'sms_master_elements.sms_master_id')
                        ->where('sms_master.id', $eventDetail->sms_master_id)
                        ->where('sms_master_elements.language', $language)
                        ->first();

                    if (empty($smsDetail)) {
                        $smsDetail  = DB::table('sms_master')
                            ->join('sms_master_elements', 'sms_master.id', 'sms_master_elements.sms_master_id')
                            ->where('sms_master.id', $eventDetail->sms_master_id)
                            ->where('sms_master_elements.language', 'EN')
                            ->first();
                    }

                    if (!empty($smsDetail)) {
                        $associatedObjects = [];
                        if (!empty($eventDetail)) {
                            $associatedObjects  = DB::table('object_master')->whereIn('id', explode(',', $smsDetail->associated_objects))->get();
                        }

                        $objectNames        = [];
                        foreach ($associatedObjects as $object) {
                            $objectNames[]  = $object->name;
                        }

                        $vars       = Common::objectElements($objectNames, 'parameters', $keys);

                        $to_numbers = strtr($smsDetail->to_mobile_no, $vars);
                        $sms        = strtr($smsDetail->sms, $vars);

                        // echo '<b>To Numbers</b> - ' . $to_numbers . '<br>';
                        // echo '<b>SMS</b> - <br>';
                        // echo '<div style="margin-left: 30px;">' . $sms . '</div>';
                        // echo '<br>----------SMS end------------<br><br>';

                        self::send_sms($to_numbers, $sms);
                    }
                }
            }

            //Notification Listing
            $eventNotificationList       = DB::table('event_master')
                ->join('event_notification_master', 'event_master.id', '=', 'event_notification_master.event_master_id')
                ->where('event_master.event_code', $event)
                ->get();

            if (!empty($eventNotificationList)) {
                foreach ($eventNotificationList as $key => $eventDetail) {
                    $notificationDetail = DB::table('notification_master')
                        ->join('notification_master_elements', 'notification_master.id', 'notification_master_elements.notification_master_id')
                        ->where('notification_master.id', $eventDetail->notification_master_id)
                        ->where('notification_master_elements.language', $language)
                        ->first();

                    if (empty($notificationDetail)) {
                        $notificationDetail = DB::table('notification_master')
                            ->join('notification_master_elements', 'notification_master.id', 'notification_master_elements.notification_master_id')
                            ->where('notification_master.id', $eventDetail->notification_master_id)
                            ->where('notification_master_elements.language', $language)
                            ->first();
                    }

                    if (!empty($notificationDetail)) {
                        $associatedObjects = [];
                        if (!empty($eventDetail)) {
                            $associatedObjects  = DB::table('object_master')->whereIn('id', explode(',', $notificationDetail->associated_objects))->get();
                        }

                        $objectNames        = [];
                        foreach ($associatedObjects as $object) {
                            $objectNames[]  = $object->name;
                        }

                        $vars           = Common::objectElements($objectNames, 'parameters', $keys);

                        $to_ids         = strtr($notificationDetail->to_ids, $vars);
                        $notification   = strtr($notificationDetail->notification, $vars);

                        $insertCompanyContacts = array();
                        $insertCompanyContacts['user_id']           = $to_ids;
                        $insertCompanyContacts['company_id']        = '';
                        $insertCompanyContacts['notification']      =  $notification;
                        $insertCompanyContacts['status']            = 0;
                        $insertCompanyContacts['link']              = '';

                        $masterModel->insertData($insertCompanyContacts, 'alert_notification');

                        // echo '<b>To Numbers</b> - ' . $to_ids . '<br>';
                        // echo '<b>Notification</b> - <br>';
                        // echo '<div style="margin-left: 30px;">' . $notification . '</div>';
                        // echo '<br>----------Notification end------------<br><br>';

                    }
                }
            }
        } catch (Exception $ex) {
             // $ErrorlogBaseController = new ErrorlogBaseController();
            // $ErrorlogBaseController->error_logging($ex,'sendAlert', 'Common.php');
            //   return view('layouts.coming_soon');
        }
    }

    public static function isValidMd5($md5 = '')
    {
        try {
            return strlen($md5) == 32 && ctype_xdigit($md5);
        } catch (Exception $ex) {
             // $ErrorlogBaseController = new ErrorlogBaseController();
            // $ErrorlogBaseController->error_logging($ex,'isValidMd5', 'Common.php');
            //   return view('layouts.coming_soon');
        }
    }

    public static function get_translation($key, $description = null, $label = null, $length = null)
    {
        try {
            $locale = App::getLocale();
            // Commented By Swapnil on 01-12-2021
            // App::setLocale($locale);
            // if (Lang::has('app.' . $key)) {
            //     return trans('app.' . $key);
            // } else {
            //     return $description;
            // }
            $lang_str = "";
            App::setLocale($locale);
            if (Lang::has('app.' . $key)) {
                $lang_str = trans('app.' . $key);
                if (!empty($length)) {
                    $lang_str = self::get_short_string($lang_str, $length, $locale);
                }
                if (!empty($label) && Lang::has('app.' . $key . '_I')) {
                    $label = trans('app.' . $key . '_I');
                    if (strlen($label) > 10) {
                        $lang_str .= ' <i class="fa fa-info-circle" title="' . $label . '"></i>';
                    }
                }
            } else {
                if (!empty($description)) {
                    $lang_str = $description;
                } else {
                    App::setLocale('en');
                    if (Lang::has('app.' . $key)) {
                        $lang_str = trans('app.' . $key);
                        if (!empty($length)) {
                            $lang_str = self::get_short_string($lang_str, $length);
                        }
                        if (!empty($label) && Lang::has('app.' . $key . '_I')) {
                            $label = trans('app.' . $key . '_I');
                            if (strlen($label) > 10) {
                                $lang_str .= ' <i class="fa fa-info-circle" title="' . $label . '"></i>';
                            }
                        }
                    } else {
                        $lang_str = $key;
                        if (!empty($length)) {
                            $lang_str = self::get_short_string($lang_str, $length);
                        }
                    }
                }
            }
            return $lang_str;
        } catch (Exception $ex) {
            // $ErrorlogBaseController = new ErrorlogBaseController();
            // $ErrorlogBaseController->error_logging($ex,'get_translation', 'Common.php');
            //   return view('layouts.coming_soon');
        }
    }
    public static function get_short_string($val, $length, $locale = null)
    {
        try {
            $stringCut = mb_substr($val, 0, $length, 'utf8');
            $endpoint = mb_strrpos($stringCut, ' ', 0, 'utf8');
            //if the string doesn't contain any space then it will cut without word basis.
            $string = $endpoint && $endpoint > $length ? mb_substr($stringCut, 0, $endpoint, 'utf8') : mb_substr($stringCut, 0);
            //$string .= ' ...';
            return $string;
        } catch (Exception $ex) {
            // $ErrorlogBaseController = new ErrorlogBaseController();
            // $ErrorlogBaseController->error_logging($ex,'get_short_string', 'Common.php');
            //   return view('layouts.coming_soon');
        }
    }

    public static function send_sms($mob_no, $message, $templateID = '')
    {
        try {
            if (!empty($templateID) && is_numeric($mob_no) && strlen($mob_no) == 10) {
                $userID         = 'collab';
                $password       = 'Collab@12';
                $senderID       = 'SMMACH';
                $apiKey         = '6036676315113144972';
                $dltEntityID    = '';
                $sendMethod     = 'simpleMsg'; //(simpleMsg|groupMsg|excelMsg)
                $messageType    = 'text'; //(text|unicode|flash)
                $scheduleTime   = ''; //mention time if you want to schedule else leave blank

                $curl = curl_init();
                curl_setopt_array($curl, array(
                    CURLOPT_URL => "http://www.smsgateway.center/SMSApi/rest/send",
                    CURLOPT_RETURNTRANSFER => true,
                    CURLOPT_ENCODING => "",
                    CURLOPT_MAXREDIRS => 10,
                    CURLOPT_TIMEOUT => 30,
                    CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                    CURLOPT_CUSTOMREQUEST => "POST",
                    CURLOPT_POSTFIELDS => "userId=$userID&password=$password&senderId=$senderID&sendMethod=$sendMethod&msgType=$messageType&mobile=$mob_no&msg=$message&duplicateCheck=true&dltEntityId=$dltEntityID&dltTemplateId=$templateID&format=json",
                    CURLOPT_HTTPHEADER => array(
                        "apikey: $apiKey",
                        "cache-control: no-cache",
                        "content-type: application/x-www-form-urlencoded"
                    ),
                ));

                $response = curl_exec($curl);
                $err = curl_error($curl);
                curl_close($curl);
                //SMSHistory::create(array('mobile'=>$mob_no, 'text'=>$message));
                //self::createSMSHistory([]);
            }
        } catch (Exception $ex) {
            // $ErrorlogBaseController = new ErrorlogBaseController();
            // $ErrorlogBaseController->error_logging($ex,'send_sms', 'Common.php');
            //   return view('layouts.coming_soon');
        }
    }

    /**
     * @param array data
     * mobile, text, user_id
     * @description This function creates the SMS history when the sms is send.
     * @return array return array including error status, message and responseData. When sms send without any issue responseData contain last inserted id.
     */
    public static function createSMSHistory($data = array())
    {
        $currentDate = Carbon::now();
        try {
            if (is_array($data)) {
                $message = '';
                $isValid = 1;
                if (empty($data['mobile'])) {
                    $message .= "Mobile is required.<br>";
                    $isValid = 0;
                }
                if (empty($data['text'])) {
                    $message .= "Text message is required.<br>";
                    $isValid = 0;
                }
                // if (empty($data['user_id'])) {
                //     $message .= "User id is required.<br>";
                //     $isValid = 0;
                // }

                if ($isValid == 1) {
                    $rows_affected = DB::table('sms_history')->insert([
                        'mobile'        => $data['mobile'],
                        'text'          => $data['text'],
                        'user_id'       => !empty($data['user_id']) ? $data['user_id'] : null,
                        'created_by'    => !empty(Auth::user()->id) ? Auth::user()->id : null,
                        'updated_by'    => !empty(Auth::user()->id) ? Auth::user()->id : null,
                        'created_at'    => $currentDate->toDateTimeString(),
                        'updated_at'    => $currentDate->toDateTimeString()
                    ]);
                    return ['error' => false, 'message' => 'Record inserted Successfully', 'responseData' => DB::getPdo()->lastInsertId()];
                } else {
                    return ['error' => true, 'message' => $message, 'responseData' => null];
                }
            } else {
                return ['error' => true, 'message' => 'Data should be of type array', 'responseData' => null];
            }
        } catch (Exception $ex) {
             // $ErrorlogBaseController = new ErrorlogBaseController();
            // $ErrorlogBaseController->error_logging($ex,'createSMSHistory', 'Common.php');
            //   return view('layouts.coming_soon');
        }
    }

    public static function moneyFormatIndia($num)
    {
        $explrestunits = "";
        if (strlen($num) > 3) {
            $lastthree = substr($num, strlen($num) - 3, strlen($num));
            $restunits = substr($num, 0, strlen($num) - 3); // extracts the last three digits
            $restunits = (strlen($restunits) % 2 == 1) ? "0" . $restunits : $restunits; // explodes the remaining digits in 2's formats, adds a zero in the beginning to maintain the 2's grouping.
            $expunit = str_split($restunits, 2);
            for ($i = 0; $i < sizeof($expunit); $i++) {
                // creates each of the 2's group and adds a comma to the end
                if ($i == 0) {
                    $explrestunits .= (int)$expunit[$i] . ","; // if is first value , convert into integer
                } else {
                    $explrestunits .= $expunit[$i] . ",";
                }
            }
            $thecash = $explrestunits . $lastthree;
        } else {
            $thecash = $num;
        }
        return $thecash; // writes the final format where $currency is the currency symbol.
    }

    public static function companyRoleManagement()
    {
        $defaultRoles   = ['Admin', 'Operator', 'Supervisor', 'Accounts', 'Project Manager', 'Purchase Manager'];
        $companies      = DB::table('company')->get();
        if (!empty($companies)) {
            foreach ($companies as $company) {
                if (!empty($defaultRoles)) {
                    foreach ($defaultRoles as $defaultRole) {
                        $roleExsDetail  = DB::table('roles')
                            ->where('role', $defaultRole)
                            ->where('company_id', $company->id)
                            ->first();

                        if (empty($roleExsDetail)) {
                            DB::table('roles')->insert([
                                'role' => $defaultRole,
                                'company_id' => $company->id,
                                'redirect_url' => 'home'
                            ]);
                        }
                    }
                }
                self::setRoleWiseAccess($company->id);
            }
        }
    }

    public static function setRoleWiseAccess($companyID)
    {
        $objectItems        = DB::table('object_items')->get();
        $companyRoles       = DB::table('roles')->where('company_id', $companyID)->get();

        //Object Item ID
        $operatorCanView    = [13];
        $operatorCanAdd     = [];
        $operatorCanEdit    = [];

        //Object Item ID
        $supervisorCanView  = [3, 7, 8, 9, 11, 12, 13, 18, 22, 28];
        $supervisorCanAdd   = [13, 28];
        $supervisorCanEdit  = [28];

        //Object Item ID
        $accountsCanView    = [3, 4, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 22, 24, 25, 26, 27, 28];
        $accountsCanAdd     = [13, 24, 25, 26, 27, 28];
        $accountsCanEdit    = [24, 25, 26, 27, 28];

        //Object Item ID
        $proMangCanView     = [1, 2, 3, 4, 5, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 22, 23, 24, 25, 26, 27, 28];
        $proMangCanAdd      = [2, 3, 4, 8, 13, 14, 15, 16, 17, 22, 26, 27, 28];
        $proMangCanEdit     = [8, 14, 15, 16, 17, 22, 28];

        //Object Item ID
        $purMangCanView     = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 22, 23, 24, 25, 26, 27, 28];
        $purMangCanAdd      = [3, 4, 7, 9, 13, 14, 15, 16, 17, 22, 28];
        $purMangCanEdit     = [14, 15, 16, 17, 22, 28];

        if (!empty($companyRoles)) {
            foreach ($objectItems as $objectDetail) {
                foreach ($companyRoles as $role) {
                    $accessDetail   = DB::table('role_management')
                        ->where('company_id', $companyID)
                        ->where('role_id', $role->id)
                        ->where('object_id', $objectDetail->object_id)
                        ->where('object_item_id', $objectDetail->id)
                        ->first();
                    if (empty($accessDetail)) {
                        if ($role->role == 'Operator') {
                            DB::table('role_management')->insert([
                                'company_id' => $companyID,
                                'role_id' => $role->id,
                                'object_id' => $objectDetail->object_id,
                                'object_item_id' => $objectDetail->id,
                                'can_view' => in_array($objectDetail->id, $operatorCanView) ? '1' : '0',
                                'can_add' => in_array($objectDetail->id, $operatorCanAdd) ? '1' : '0',
                                'can_edit' => in_array($objectDetail->id, $operatorCanEdit) ? '1' : '0'
                            ]);
                        }
                        if ($role->role == 'Supervisor') {
                            DB::table('role_management')->insert([
                                'company_id' => $companyID,
                                'role_id' => $role->id,
                                'object_id' => $objectDetail->object_id,
                                'object_item_id' => $objectDetail->id,
                                'can_view' => in_array($objectDetail->id, $supervisorCanView) ? '1' : '0',
                                'can_add' => in_array($objectDetail->id, $supervisorCanAdd) ? '1' : '0',
                                'can_edit' => in_array($objectDetail->id, $supervisorCanEdit) ? '1' : '0'
                            ]);
                        }
                        if ($role->role == 'Accounts') {
                            DB::table('role_management')->insert([
                                'company_id' => $companyID,
                                'role_id' => $role->id,
                                'object_id' => $objectDetail->object_id,
                                'object_item_id' => $objectDetail->id,
                                'can_view' => in_array($objectDetail->id, $accountsCanView) ? '1' : '0',
                                'can_add' => in_array($objectDetail->id, $accountsCanAdd) ? '1' : '0',
                                'can_edit' => in_array($objectDetail->id, $accountsCanEdit) ? '1' : '0'
                            ]);
                        }
                        if ($role->role == 'Purchase Manager') {
                            DB::table('role_management')->insert([
                                'company_id' => $companyID,
                                'role_id' => $role->id,
                                'object_id' => $objectDetail->object_id,
                                'object_item_id' => $objectDetail->id,
                                'can_view' => in_array($objectDetail->id, $proMangCanView) ? '1' : '0',
                                'can_add' => in_array($objectDetail->id, $proMangCanAdd) ? '1' : '0',
                                'can_edit' => in_array($objectDetail->id, $proMangCanEdit) ? '1' : '0'
                            ]);
                        }
                        if ($role->role == 'Project Manager') {
                            DB::table('role_management')->insert([
                                'company_id' => $companyID,
                                'role_id' => $role->id,
                                'object_id' => $objectDetail->object_id,
                                'object_item_id' => $objectDetail->id,
                                'can_view' => in_array($objectDetail->id, $purMangCanView) ? '1' : '0',
                                'can_add' => in_array($objectDetail->id, $purMangCanAdd) ? '1' : '0',
                                'can_edit' => in_array($objectDetail->id, $purMangCanEdit) ? '1' : '0'
                            ]);
                        }
                        if ($role->role == 'Admin') {
                            DB::table('role_management')->insert([
                                'company_id' => $companyID,
                                'role_id' => $role->id,
                                'object_id' => $objectDetail->object_id,
                                'object_item_id' => $objectDetail->id,
                                'can_view' => '1',
                                'can_add' => '1',
                                'can_edit' => '1'
                            ]);
                        }
                    }
                }
            }
        }
    }

    public static function checkAccess($menuID)
    {
        $authDetail = Auth::user();

        $detail     = DB::table('role_management')
            ->leftJoin('object_items', 'role_management.object_item_id', '=', 'object_items.id')
            ->where('role_management.company_id', $authDetail->company_id)
            ->where('role_management.role_id', $authDetail->role_id)
            ->where('object_items.menu_id', $menuID)
            ->first();
        if (!empty($detail) && $detail->can_view == '0') {
            return false;
        }
        return true;
    }

    public static function checkAddEditAccess($code, $companyID, $roleID)
    {
        try {
            $settings['add_btn_status'] = 1;
            $settings['edt_btn_status'] = 1;
            $accessData                 = DB::table('role_management')
                ->join('roles', 'role_management.role_id', '=', 'roles.id')
                ->join('object_items', 'role_management.object_item_id', '=', 'object_items.id')
                ->where('roles.id', $roleID)
                ->where('object_items.code', $code)
                ->where('roles.company_id', $companyID)
                ->first();
            if (!empty($accessData)) {
                $settings['add_btn_status'] = $accessData->can_add;
                $settings['edt_btn_status'] = $accessData->can_edit;
            }

            return $settings;
        } catch (Exception $ex) {
             // $ErrorlogBaseController = new ErrorlogBaseController();
            // $ErrorlogBaseController->error_logging($ex,'checkAddEditAccess', 'Common.php');
            //   return view('layouts.coming_soon');
        }
    }

    public static function generateReferralID($limit = 9)
    {
        try {
            $code   = Str::random($limit);
            $cnt    = DB::table('users')
                ->where('referral_id', $code)
                ->count();
            if ($cnt > 0) {
                self::generateReferralID($limit);
            } else {
                return $code;
            }
        } catch (\Exception $ex) {
            // $ErrorlogBaseController = new ErrorlogBaseController();
            // $ErrorlogBaseController->error_logging($ex,'generateReferralID', 'Common.php');
            //   return view('layouts.coming_soon');
        }
    }

    public static function generateReferralLink($url, $type = 'NA')
    {
        try {
            $auth = Auth::user();

            if (empty($auth->referral_id)) {
                $code = self::generateReferralID('9');
                DB::table('users')
                    ->where('id', $auth->id)
                    ->update(['referral_id' => $code]);
            } else {
                $code = $auth->referral_id;
            }
            //return url('/referral-program?_r=' . urlencode($url) . '&_t=' . $type . '&_c=' . $auth->referral_id);
            return url('/referral-program/' . urlencode($url) . '/' . $type . '/' . $code);
        } catch (Exception $ex) {
            // $ErrorlogBaseController = new ErrorlogBaseController();
            // $ErrorlogBaseController->error_logging($ex,'generateReferralLink', 'Common.php');
            //   return view('layouts.coming_soon');
        }
    }

    public static function referralActivity($type, $userID)
    {
        try {
            $referralID = Session::get('referral_id');

            if (!empty($referralID)) {
                /*DB:table('referral_activity')->insert([
                    'referral_id' => $referralID,
                    'connected_user_id' => $userID,
                    'activity_type' => $type,
                    'reward_point' => '10'
                ]);*/
                DB::table('referral_activity')->insert([
                    'referral_id' => $referralID,
                    'connected_user_id' => $userID,
                    'activity_type' => $type,
                    'reward_point' => '10'
                ]);
            }
        } catch (Exception $ex) {
             // $ErrorlogBaseController = new ErrorlogBaseController();
            // $ErrorlogBaseController->error_logging($ex,'referralActivity', 'Common.php');
            //   return view('layouts.coming_soon');
        }
    }


    public static function getAccordionMenus($shortCode)
    {
        try {
            $accordion_menu = DB::table('menu_mapping')
                ->select('menu_mapping.id', 'menu_mapping.menu_id', 'menu_mapping.short_code', 'menu_mapping.description', 'menu_mapping.url', 'menu_mapping.class', 'menu_mapping.font_icon_class', 'menus.language_key')
                ->join('menus', 'menu_mapping.menu_id', '=', 'menus.id')
                ->where('menu_mapping.is_deleted', '=', '0')
                ->whereRaw("menu_mapping.parent_id = (select id from menu_mapping where short_code = '$shortCode')")
                ->orderBy('sequence', 'ASC')
                ->get();

            return $accordion_menu;
        } catch (Exception $ex) {
             // $ErrorlogBaseController = new ErrorlogBaseController();
            // $ErrorlogBaseController->error_logging($ex,'getAccordionMenus', 'Common.php');
            //   return view('layouts.coming_soon');

        }
    }

    // Get Attachment Description string by passing array of attachments
    public static function getAttachmentDetailsFromId($attachment_ids = array())
    {
        try {
            $string = '';
            if (is_array($attachment_ids)) {
                foreach ($attachment_ids as $attachment_id) {
                    $data = DB::table('attachment_master')->select('machine_attachment')->where('id', '=', $attachment_id)->first();
                    $string .= !empty($data->machine_attachment) ? $data->machine_attachment : 'Any';
                    if (next($attachment_ids)) {
                        $string .= ", ";
                    }
                }
            }
            return $string;
        } catch (Exception $ex) {
            // $ErrorlogBaseController = new ErrorlogBaseController();
            // $ErrorlogBaseController->error_logging($ex,'getAttachmentDetailsFromId', 'Common.php');
            //   return view('layouts.coming_soon');
        }
    }

    // Get Attachment Description string by passing array of attachments
    public static function getModelDetailsFromId($model_ids = array())
    {
        try {
            $string = '';
            if (is_array($model_ids)) {
                foreach ($model_ids as $model_id) {
                    $data = DB::table('machine_master')->select('machine_model')->where('id', '=', $model_id)->first();
                    $string .= !empty($data->machine_model) ? $data->machine_model : 'Any';
                    if (next($model_ids)) {
                        $string .= ", ";
                    }
                }
            }
            return $string;
        } catch (Exception $ex) {
             // $ErrorlogBaseController = new ErrorlogBaseController();
            // $ErrorlogBaseController->error_logging($ex,'getModelDetailsFromId', 'Common.php');
            //   return view('layouts.coming_soon');
        }
    }

    // Get Make Description string by passing array of make
    public static function getMakesDetailsFromId($make_ids = array())
    {
        try {
            $string = '';
            if (is_array($make_ids)) {
                foreach ($make_ids as $make_id) {
                    $data = DB::table('machine_make_master')->select('make_name')->where('id', '=', $make_id)->first();
                    $string .= !empty($data->make_name) ? $data->make_name : 'Any';
                    if (next($make_ids)) {
                        $string .= ", ";
                    }
                }
            }
            return $string;
        } catch (Exception $ex) {
            // $ErrorlogBaseController = new ErrorlogBaseController();
            // $ErrorlogBaseController->error_logging($ex,'getMakesDetailsFromId', 'Common.php');
            //   return view('layouts.coming_soon');
        }
    }

    // Get M/c Capacity Description string by passing array of capacity
    public static function getCapacitiesFromId($capacity_ids = array())
    {
        try {
            $string = '';
            if (is_array($capacity_ids)) {
                foreach ($capacity_ids as $capacity_id) {
                    $data = DB::table('capacity_master')->select('capacity')->where('id', '=', $capacity_id)->first();
                    $string .= !empty($data->capacity) ? $data->capacity : 'Any';
                    if (next($capacity_ids)) {
                        $string .= ", ";
                    }
                }
            }
            return $string;
        } catch (Exception $ex) {
             // $ErrorlogBaseController = new ErrorlogBaseController();
            // $ErrorlogBaseController->error_logging($ex,'getCategoryFromId', 'Common.php');
            //   return view('layouts.coming_soon');
        }
    }

    // Get M/c Category Description string by passing array of category
    public static function getCategoryFromId($category_ids = array())
    {
        try {
            $string = '';
            if (is_array($category_ids)) {
                foreach ($category_ids as $category_id) {
                    $data = DB::table('machine_category_master')->select('name')->where('id', '=', $category_id)->first();
                    $string .= !empty($data->name) ? $data->name : 'Any';
                    if (next($category_ids)) {
                        $string .= ", ";
                    }
                }
            }
            return $string;
        } catch (Exception $ex) {
            // $ErrorlogBaseController = new ErrorlogBaseController();
            // $ErrorlogBaseController->error_logging($ex,'getCategoryFromId', 'Common.php');
            //   return view('layouts.coming_soon');
        }
    }

    /*
    @ Return Subject for message.
    */

    public static function messageSubject($machine_id, $service_type_code)
    {
        try {
            switch ($service_type_code) {
                case 'REN':
                    $query = DB::table('machine_basic_detail')
                        ->select(
                            DB::raw('CONCAT_WS(" ",machine_category_master.name, "",machine_master.machine_model,
                                " | YOM: ",machine_basic_detail.model_year,"| Location: ", machine_basic_detail.current_location
                                ) as subject')
                        )
                        ->leftJoin('machine_master', 'machine_master.id', 'machine_basic_detail.machine_model_id')
                        ->leftJoin('machine_category_master', 'machine_category_master.id', 'machine_basic_detail.machine_category_id')
                        ->leftJoin('capacity_master', 'capacity_master.id', '=', 'machine_basic_detail.machine_capacity_id');
                    $query->where('machine_basic_detail.rental', '1');
                    $subjectData = $query->where('machine_basic_detail.id', $machine_id)->first();
                    $data['subject'] = $subjectData->subject;
                    break;
                case 'RES':
                    $query = DB::table('machine_basic_detail')
                        ->select(
                            DB::raw('CONCAT_WS(" ",machine_category_master.name, "",machine_master.machine_model,
                                " | YOM: ",machine_basic_detail.model_year,"| Location: ", machine_basic_detail.current_location
                                ) as subject')
                        )
                        ->leftJoin('machine_master', 'machine_master.id', 'machine_basic_detail.machine_model_id')
                        ->leftJoin('machine_category_master', 'machine_category_master.id', 'machine_basic_detail.machine_category_id')
                        ->leftJoin('capacity_master', 'capacity_master.id', '=', 'machine_basic_detail.machine_capacity_id');
                    $query->where('machine_basic_detail.resale', '1');
                    $subjectData = $query->where('machine_basic_detail.id', $machine_id)->first();
                    $data['subject'] = $subjectData->subject;
                    break;
                case 'CONTACT_US':
                    $collabData = DB::table('users')->where('role_id', 4)->first();

                    $data['to_user_id']     = $collabData->id;
                    $data['to_company_id']  = $collabData->company_id;
                    $data['subject'] = 'Contact Us.';
                    break;
            }
            return $data;
        } catch (\Exception $ex) {
              // $ErrorlogBaseController = new ErrorlogBaseController();
            // $ErrorlogBaseController->error_logging($ex,'messageSubject', 'Common.php');
            //   return view('layouts.coming_soon');
        }
    }

    /*
    @ Return Subject for message.
    */
    public static function thank_you_message($to_company_id, $code)
    {
        try {

            switch ($code) {
                case 'MAC':
                    $ownerData = DB::table('users')->where('company_id', $to_company_id)->first();

                    $data['owner_name'] = $ownerData->name;
                    $ownerName = $ownerData->name;
                    break;
                case 'CONTACT_US':
                    $ownerName = 'SharedMachine Team';
                    break;
                case 'DLY-REN':
                    $ownerName = 'SharedMachine Team';
                    break;
            }
            $data['msg'] = '<span class="black_color font_weight400 font_14px">Your enquiry has been sent to <span class="t_color">' . $ownerName . '</span>.<br>To check further replies from the owner.</span>';
            return $data;
        } catch (\Exception $ex) {
             // $ErrorlogBaseController = new ErrorlogBaseController();
            // $ErrorlogBaseController->error_logging($ex,'thank_you_message', 'Common.php');
            //   return view('layouts.coming_soon');
        }
    }
    public static function event_logging($eventlogid, $event, $type, $language)
    {
        try {
            $currentDate = Carbon::now();
            $event_log = array(
                'event_log_id' => $eventlogid,
                'date' => date('Y-m-d'),
                'time' => date('H:i:s'),
                'code' => $event,
                'event_type' => $type,
                'language' => $language,
                'status' => 'Failed',
                'created_by' => !empty(Auth::user()->id) ? Auth::user()->id : null,
                'created_at' => $currentDate->toDateTimeString()
            );
            $masterModel  = new MasterModel();
            $rows_affected = $masterModel->insertData($event_log, 'event_logs');
            return $rows_affected;
        } catch (Exception $ex) {
              // $ErrorlogBaseController = new ErrorlogBaseController();
            // $ErrorlogBaseController->error_logging($ex,'event_logging', 'Common.php');
            //   return view('layouts.coming_soon');
        }
    }

    public static function update_event_logging($eventsummarylog_id, $eventlogid, $userids, $parameter, $description, $type, $subject, $language, $cc_emails = null)
    {
        try {
            $currentDate = Carbon::now();
            $event_log = array(
                'notify_user_id' => $userids,
                'user_details' => $parameter,
                'event_title' => $subject,
                'event_description' => $description,
                'language' => $language,
                'status' => 'Success',
                'updated_by' => !empty(Auth::user()->id) ? Auth::user()->id : null,
                'updated_at' => $currentDate->toDateTimeString()
            );
            if (!empty($cc_emails)) {
                $event_log['user_details_extra'] = $cc_emails;
            }
            $masterModel               = new MasterModel();
            $rows_affected = $masterModel->updateData($event_log, 'event_logs', ['id' => $eventlogid]);
            if ($rows_affected) {
                if ($type == 'Email') {
                    DB::table('event_summary_logs')->where('id', $eventsummarylog_id)->increment('email_send', 1);
                } elseif ($type == 'SMS') {
                    DB::table('event_summary_logs')->where('id', $eventsummarylog_id)->increment('sms_sent', 1);
                } elseif ($type == 'Notification') {
                    DB::table('event_summary_logs')->where('id', $eventsummarylog_id)->increment('notification_sent', 1);
                }
            }
            return $rows_affected;
        } catch (Exception $ex) {
             // $ErrorlogBaseController = new ErrorlogBaseController();
            // $ErrorlogBaseController->error_logging($ex,'update_event_logging', 'Common.php');
            //   return view('layouts.coming_soon');
        }
    }
    public static function event_summary_logging($event, $keys, $event_count, $sms_count, $notification_count)
    {
        try {
            $currentDate = Carbon::now();
            $event_summary_log = array(
                'date' => date('Y-m-d'),
                'time' => date('H:i:s'),
                'event_code' => $event,
                // 'event_parameter' => implode(',', array_keys($keys)),
                'no_of_emails' => $event_count,
                'email_send' => 0,
                'no_of_sms' => $sms_count,
                'sms_sent' => 0,
                'no_of_notifications' => $notification_count,
                'notification_sent' => 0,
                'status' => 'Failed',
                'created_by' => !empty(Auth::user()->id) ? Auth::user()->id : null,
                'created_at' => $currentDate->toDateTimeString()
            );
            $parameters = array();
            if (!empty($keys)) {
                foreach ($keys as $k => $v) {
                    $parameters[] = $k . '=' . $v;
                }
            }
            $event_summary_log['event_parameter'] = implode(',', $parameters);
            $masterModel = new MasterModel();
            $rows_affected = $masterModel->insertData($event_summary_log, 'event_summary_logs');
            return $rows_affected;
        } catch (Exception $ex) {
                // $ErrorlogBaseController = new ErrorlogBaseController();
            // $ErrorlogBaseController->error_logging($ex,'event_summary_logging', 'Common.php');
            //   return view('layouts.coming_soon');
        }
    }
    public static function update_event_summary_status($eventlogid)
    {
        try {
            $currentDate = Carbon::now();
            $event_summary_log = array(
                'status' => 'Success',
                'updated_by' => !empty(Auth::user()->id) ? Auth::user()->id : null,
                'updated_at' => $currentDate->toDateTimeString()
            );
            $masterModel = new MasterModel();
            $rows_affected = $masterModel->updateData($event_summary_log, 'event_summary_logs', ['id' => $eventlogid]);
            return $rows_affected;
        } catch (Exception $ex) {
                // $ErrorlogBaseController = new ErrorlogBaseController();
            // $ErrorlogBaseController->error_logging($ex,'update_event_summary_status', 'Common.php');
            //  return view('layouts.coming_soon');
        }
    }
}
