$(document).ready(function () {

    $('[data-toggle="tooltip"]').tooltip();

    $(".allownumericwithoutdecimal").on("keypress keyup blur", function (event) {
        if ((event.which < 48 || event.which > 57)) {
            event.preventDefault();
        }
    });

    $(".allownumericwithdecimal").on("keypress keyup blur", function (event) {
        if ((event.which != 46 || $(this).val().indexOf('.') != -1) && (event.which < 48 || event.which > 57)) {
            event.preventDefault();
        }
    });

    $(".chosen-container").addClass("border-sky-blue");
    var config = {
        '.chosen-select': {},
        '.chosen-select-deselect': { allow_single_deselect: true },
        '.chosen-select-no-single': { disable_search_threshold: 10 },
        '.chosen-select-no-results': { no_results_text: 'Oops, nothing found!' },
    }

    for (var selector in config) {
        try {
            $(selector).chosen(config[selector]);
        } catch (err) {
            for (var prop in err) {
                console.log("property: " + prop + " value: [" + err[prop] + "]\n");
            }
        }
    }

    try {
        $(".chosen-select").chosen().change(function () {
            $(this).val()
        });
    } catch (err) {
        for (var prop in err) {
            console.log("property: " + prop + " value: [" + err[prop] + "]\n");
        }
    }

    /*
    @ Table header set sorting order.
    */

    $(document).on('click', '.header', function (e) {
        var me = $(this);
        e.preventDefault();
        var obj = $(this);

        var filterBy = $(this).data('filterby');
        //changed to include current sort.
        var currentSort = $(this).closest('.articles').find('.sorting_method').val();
        var sort = '';
        if (currentSort == 'asc') {
            sort = 'desc';
        } else {
            sort = 'asc';
        }

        var sorting_url = $(this).data('sorting_url').split('?');
        var sPageURL = decodeURIComponent(window.location.search.substring(1));
        var sURLVariables = sPageURL.split('&');

        $('.loading').show();
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        if (me.data('requestRunning')) {
            return;
        }

        me.data('requestRunning', true);

        $.ajax({
            url: sorting_url[0] + '?' + sURLVariables[0],
            type: 'get',
            data: { filter_column_name: filterBy, sorting_method: sort },
            success: function (response) {

                obj.closest(".articles").html(response);

                var config = {
                    '.chosen-select': {},
                    '.chosen-select-deselect': { allow_single_deselect: true },
                    '.chosen-select-no-single': { disable_search_threshold: 10 },
                    '.chosen-select-no-results': { no_results_text: 'Oops, nothing found!' },
                }

                for (var selector in config) {
                    $(selector).chosen(config[selector]);
                }

                $(".chosen-select").chosen().change(function () {
                    $(this).val()
                });

                if (sort == "asc") {
                    $('.sorting_method').val("asc");
                } else {
                    $('.sorting_method').val("desc");
                }
                $('.loading').hide();
            },
            complete: function () {
                me.data('requestRunning', false);
            }
        });
    });

    /*
     @ AJAX Call for add menu in menu mapping.
     */
    $('.addToMenuList').on('click', function () {
        var menuID = $(this).data('menuid');
        var menu_name = getUrlParameter('menu_name');
        // console.log(menu_name);

         if (menu_name != undefined || menu_name != '') {
            $.ajax({
                type: 'GET',
                url: APP_URL + '/addMenuMappingItem',
                data: { 'menuID': menuID, 'menu_name': menu_name },
                success: function (data) {
                   console.log(data);
                    alert(data['msg']);
                    location.reload();
                }
            });
        }else{
            alert('Please select menu type');
        }
    });

    $('.user-menu-access-control').on('click', function () {

        var parentID = $(this).data('parentid');
        var mappingID = $(this).data('mappingid');
        var menuID = $(this).data('menuid');
        var url = $(this).data('url');
        var heading = $(this).data('heading');
        //var roleID      = $(this).data('roleid');


        $('#menu-editor').hide();
        $.ajax({
            type: 'GET',
            url: APP_URL + '/editUserAccessRole',
            data: { 'menuID': menuID, 'parentID': parentID, 'mappingID': mappingID, 'url': url, 'heading': heading },
            success: function (data) {
                //location.reload();
                $('#access-control').html(data)
                //console.log(data);
            }
        });
    });

    $('.menu-access-control').on('click', function () {
        if ($('#active_menu').length) {
            // if ($(".access-controls").hasClass('display-none')) {
            //     $(".access-controls").removeClass('display-none');
            // }
            var menu_name = $('#active_menu').val();
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            $.ajax({
                type: 'GET',
                url: APP_URL + '/userAccessRole',
                data: { 'menu_name': menu_name },
                success: function (data) {
                    //location.reload();
                    $('#access-control').html(data)
                    //console.log(data);
                }
            });
        } else {
            alert('Please select one Parent Menu to set access control');
        }
    });


    $('body').on('change', '.change-access-type', function () {
        var url = $(this).find(':selected').data('url');
        var menuID = $(this).find(':selected').data('menuid');
        var parentID = $(this).find(':selected').data('parentid');
        var mappingID = $(this).find(':selected').data('mappingid');
        var roleID = $(this).find(':selected').data('roleid');
        var accessID = $(this).val();

        $.ajax({
            type: 'GET',
            url: APP_URL + '/updateUserAccessRole',
            data: { 'menuID': menuID, 'parentID': parentID, 'mappingID': mappingID, 'url': url, 'accessID': accessID, 'roleID': roleID },
            success: function (data) {
                //location.reload();
                $('#access-control').html(data)
                //console.log(data);
            }
        });
        console.log(url + ' ' + menuID + ' ' + parentID + ' ' + mappingID);
    });

    /*Export Data*/
    // $('.export_buttons').on('click', function() {
    //     var url = $(this).data('url');


    //     $.ajax({
    //         type: 'GET',
    //         url: APP_URL + '/export_to_excel1',
    //         data: { url: url },
    //         enctype: 'multipart/form-data',
    //         success: function(data) {

    //         }
    //     });
    // });



    $('.userMasterEdit').click(function () {
        //   alert("Here");
        $('#myModalEdit').modal('show');

        $("#myModalEdit #delete").hide();
        $("#myModalEdit #restore").hide();

        $("#myModalEdit").modal('show');
        $("#myModalEdit #blah").attr('src', 'storage/' + $(this).attr("data-profile_pic"));
        $("#myModalEdit #id").val($(this).attr("data-id"));
        $("#myModalEdit .ddlCompanyGetRolesAndLanguages").val($(this).attr("data-company_id")).trigger('change');
        $("#myModalEdit #name").val($(this).attr("data-name"));
        $("#myModalEdit #email").val($(this).attr("data-email"));
        $("#myModalEdit .language_id").val($(this).attr("data-language_id"));
        $("#myModalEdit .role_id").val($(this).attr("data-role_id")).trigger('change');
        $("#myModalEdit #mobile_no").val($(this).attr("data-mobile_no"));

        //s   $("#myModalEdit .city").val($(this).attr("data-ctid"));

        //var city_id = $(this).attr("data-ctid");
        //console.log(city_id);

        //  $("#EditModal #test_city").selectedIndex = city_id;


        if ($(this).attr("data-status") == 0) {
            $("#myModalEdit #delete").show();
        }
        if ($(this).attr("data-status") == 1) {
            $("#myModalEdit #restore").show();
        }
    });


    $('.reportrange').on('apply.daterangepicker', function (ev, picker) {
        $('#filter_from_date').val(picker.startDate.format('YYYY-MM-DD'));
        $('#filter_to_date').val(picker.endDate.format('YYYY-MM-DD'));
    });

    $(function () {
        try {
            var start = moment().subtract(29, 'days');
            var end = moment();

            function cb(start, end) {

                //$('.reportrange span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'));
            }

            $('.reportrange').daterangepicker({
                startDate: start,
                endDate: end,
                ranges: {
                    'Today': [moment(), moment()],
                    'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
                    'Last 7 Days': [moment().subtract(6, 'days'), moment()],
                    'Last 30 Days': [moment().subtract(29, 'days'), moment()],
                    'This Month': [moment().startOf('month'), moment().endOf('month')],
                    'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
                }
            }, cb);
            cb(start, end);
            try {
                var start = moment().subtract(29, 'days');
                var end = moment();

                var from_date_parts = $('#filter_from_date').val().split('-');
                var format_from_date = moment(from_date_parts[2] + '.' + from_date_parts[1] + '.' + from_date_parts[0], "DD.MM.YYYY").format("MMMM D, YYYY");

                var to_date_parts = $('#filter_to_date').val().split('-');
                var format_to_date = moment(to_date_parts[2] + '.' + to_date_parts[1] + '.' + to_date_parts[0], "DD.MM.YYYY").format("MMMM D, YYYY");

                var from_date = format_from_date != 'Invalid date' ? format_from_date : start.format('MMMM D, YYYY');
                var to_date = format_to_date != 'Invalid date' ? format_to_date : end.format('MMMM D, YYYY');

                //$('.reportrange span').html(from_date + ' - ' + to_date);
            } catch (e) {
                for (var prop in e) {
                    console.log('In DatePicker');
                    console.log("property: " + prop + " value: [" + e[prop] + "]\n");
                }
            }

            $('.reportrange').on('apply.daterangepicker', function (ev, picker) {
                $('.reportrange span').html(picker.startDate.format('MMMM D, YYYY') + ' - ' + picker.endDate.format('MMMM D, YYYY'));
            });
        } catch (err) {
            for (var prop in err) {
                console.log('In DatePicker');
                console.log("property: " + prop + " value: [" + err[prop] + "]\n");
            }
        }
    });




    /*-- Depending drop down ajax call --*/
    $('.dependingDropDown').on('change', function () {
        var id = $(this).val().toString();
        var child_dd_id = $(this).data('child-dd_id');
        var str = '';

        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        $.ajax({
            type: 'GET',
            url: APP_URL + '/depending_dropdown',
            data: { 'id': id, child_dd_id: child_dd_id },
            success: function (ret_data) {

                if (ret_data.length > 0) {
                    str = '';
                    $.each(ret_data, function (key, val) {
                        str += '<option value="' + val['value'] + '">' + val['label'] + '</option>';
                    });

                    $('.select-' + child_dd_id).html(str);
                    $('.select-' + child_dd_id).trigger("chosen:updated");
                    // console.log(str);
                } else {
                    $('.select-' + child_dd_id).html(str);
                    $('.select-' + child_dd_id).trigger("chosen:updated");
                }
            }
        });
    });

});

/*-- Pagination --*/
var request = null;
$(document).on('click', '.pagination a', function (event) {
    var url = $(this).attr('href');
    var search_string = $('#search-box').val();

    $('.loading').show();
    event.preventDefault();
    fetch_data(url, $(this), search_string);
});

function fetch_data(url, obj, search_string) {
    // this is final pagination - do not touch the code
    // Jul 22, 2020

    console.log('obj', obj);

    var list_id = getUrlParameter('list_id');
    var service_type = getUrlParameter('service_type');
    var listing_service_type = getUrlParameter('service');
    if (service_type == undefined || service_type == null || service_type == '' || service_type == 'undefined') {
        service_type = listing_service_type;
    } else if (listing_service_type == undefined || listing_service_type == null || listing_service_type == '' || listing_service_type == 'undefined') {
        service_type = service_type;
    } else {
        service_type = '';
    }

    var filter_column_name = obj.closest('.articles').find('.filter_column_name').val();;
    var sorting_method = obj.closest('.articles').find('.sorting_method').val();

    // console.log('filter_column_name',filter_column_name);
    // console.log('sorting_method',sorting_method);
    // Added by Shrikant 13-Jan-20. Prevent to send multiple ajax request (Line no. 368 to 370).
    if (request && request.readyState != 4) {
        //request.abort();
        return;
    }
    request = $.ajax({
        method: 'get',
        url: url,
        // data: { 'data': $('.filter').serialize(),'basics' :search_string, 'list_id' : list_id, 'service' : service_type},
        data: { 'filter_column_name': filter_column_name, 'sorting_method': sorting_method, 'basics': search_string, 'list_id': list_id, 'service': service_type },
        //data: JSON.parse(JSON.stringify(filterData)),
        success: function (data) {
            obj.closest('.accordion-list').html(data);
            $('.loading').hide();

            /*-- Set all filter after pagination --*/
            var config = {
                '.chosen-select': {},
                '.chosen-select-deselect': { allow_single_deselect: true },
                '.chosen-select-no-single': { disable_search_threshold: 10 },
                '.chosen-select-no-results': { no_results_text: 'Oops, nothing found!' },
            }

            for (var selector in config) {
                $(selector).chosen(config[selector]);
            }

            $(".chosen-select").chosen().change(function () {
                $(this).val()
            });

            /*-- Set date after pagination --*/
            var start = moment().subtract(29, 'days');
            var end = moment();

            function cb(start, end) {
                $('.reportrange span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'));
            }

            $('.reportrange').daterangepicker({
                startDate: start,
                endDate: end,
                ranges: {
                    'Today': [moment(), moment()],
                    'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
                    'Last 7 Days': [moment().subtract(6, 'days'), moment()],
                    'Last 30 Days': [moment().subtract(29, 'days'), moment()],
                    'This Month': [moment().startOf('month'), moment().endOf('month')],
                    'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
                }
            }, cb);

            cb(start, end);

        }
    });

}
/*
@ Add Modal Open
*/
function addModuleOpen(type, obj) {
    if (type == 'listing') {
        var formKey = $('#addEditFormKey').val();
        console.log('listing formKey=', formKey);

    } else if (type == 'accordion') {
        var formKey = obj.closest('.accordion-start').find('#addEditFormKey').val();
        console.log('accordion formKey=', formKey);
    }
    $('#modalContent').empty();
    $.ajax({
        type: 'GET',
        url: APP_URL + '/generate_form?key=' + formKey + '&id=0',
        success: function (data) {

            $('#modalContent').append(data);

            modalDataBindings = 1;
        }
    });
}

/*
@ Custom Add Module Open
*/
function addCustomModuleOpen(controller, function_name, list_id, service_type) {
    console.log(list_id);
    console.log(service_type);
    $('#modalContent').empty();
    $.ajax({
        type: 'GET',
        url: APP_URL + '/' + controller + '/' + function_name,
        data: { list_id: list_id, service_type: service_type },

        success: function (data) {

            $('#modalContent').append(data);

            modalDataBindings = 1;
        }
    });
}

/*
@ Use for Pass multiple values with action icon.
*/
function addCustomModuleOpenWithParameters(controller, function_name, extra_parameter = {}) {
    var parameter = '';
    $('#modalContent').empty();
    $.ajax({
        type: 'GET',
        url: APP_URL + '/' + controller + '/' + function_name,
        data: extra_parameter,
        success: function (data) {

            $('#modalContent').append(data);

            modalDataBindings = 1;
        }
    });
}

function approve(controller, function_name, row_id, service_type, list_id) {
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
    $.ajax({
        type: 'POST',
        url: APP_URL + '/' + controller + '/' + function_name,
        data: { row_id: row_id, service_type: service_type, list_id: list_id, status: 'approve' },
        success: function (data) {
            alert('Approve Successfully');
            location.reload();
        }
    });
}
/*
@ Custom Edit Module Open
*/
function editCustomModuleOpen(controller, function_name, row_id, service_type, list_id) {
    $('#modalContent').empty();
    $.ajax({
        type: 'GET',
        url: APP_URL + '/' + controller + '/' + function_name,
        data: { row_id: row_id, service_type: service_type, list_id: list_id },
        success: function (data) {

            $('#modalContent').append(data);

            modalDataBindings = 1;

            // console.log('reloading file');
            $.getScript("/js/common.js", function (data, textStatus, jqxhr) { });
        }
    });
}
/*
@ Use for Pass multiple values with action icon.
*/
function editCustomModuleOpenWithParameters(controller, function_name, extra_parameter = {}) {
    var parameter = '';
    $('#modalContent').empty();
    $.ajax({
        type: 'GET',
        url: APP_URL + '/' + controller + '/' + function_name,
        data: extra_parameter,
        success: function (data) {

            $('#modalContent').append(data);

            modalDataBindings = 1;

            $.getScript("/js/common.js", function (data, textStatus, jqxhr) { });
        }
    });
}


/*-- Edit Model Open --*/
function editEntry(id, obj) {
    var formKey = obj.closest('.accordion-start').find('#addEditFormKey').val();
    var multiSaveKey = obj.closest('.accordion-start').find('#multiSaveKey').val();

    if (formKey == undefined) {
        var formKey = $('#addEditFormKey').val();
        var multiSaveKey = $('#multiSaveKey').val();
    }
    $('#modalContent').empty();
    // console.log('Form Key==-', formKey);
    // console.log('Multi Save Key=', multiSaveKey);
    $.ajax({
        type: 'GET',
        url: APP_URL + '/generate_form?key=' + formKey + '&id=' + id + '&multiSaveKey=' + multiSaveKey,
        success: function (data) {
            $('#modalContent').append(data);
            $.getScript("/js/common.js", function (data, textStatus, jqxhr) { });
        }
    });
}
/*
@ Custom Delete Module Open
*/
function deleteCustomModule(controller, function_name, list_id) {
    $('#modalContent').empty();
    $.ajax({
        type: 'GET',
        url: APP_URL + '/' + controller + '/' + function_name,
        data: { list_id: list_id },
        success: function (data) {
            $('#modalContent').append(data);
            modalDataBindings = 1;
            // console.log('reloading file');
            $.getScript("/js/common.js", function (data, textStatus, jqxhr) { });
        }
    });
}


/***** A C C E P T  E X C E L  F I L E  O N L Y
 * <input type="file" id="file" onchange="CheckFile(this);" />
 */
function CheckFile(sender) {
    var validExts = new Array(".xlsx", ".xls", ".csv");
    var fileExt = sender.value;
    fileExt = fileExt.substring(fileExt.lastIndexOf('.'));
    if (validExts.indexOf(fileExt) < 0) {
        alert("Invalid file selected, valid files are of " +
            validExts.toString() + " types.");
        return false;
    } else return true;
}

/* Convert Sql Date Formate to dd mmm YYYY */

function getFormattedDate(input) {

    var Splitdate = input.split('-');
    var newDate = Splitdate[1] + '/' + Splitdate[2] + '/' + Splitdate[0];

    var pattern = /(.*?)\/(.*?)\/(.*?)$/;
    var result = newDate.replace(pattern, function (match, p1, p2, p3) {
        var months = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];
        return (p2 < 10 ? p2 : p2) + " " + months[(p1 - 1)] + " " + p3;
    });
    return result;
}
// // Return date to yyyy-MM-dd
// function formatDate(date) {
//     var d = new Date(date),
//         month = '' + (d.getMonth() + 1),
//         day = '' + d.getDate(),
//         year = d.getFullYear();

//     if (month.length < 2)
//         month = '0' + month;
//     if (day.length < 2)
//         day = '0' + day;

//     return [year, month, day].join('-');
// }

var getUrlParameter = function getUrlParameter(sParam) {
    var sPageURL = decodeURIComponent(window.location.search.substring(1)),
        sURLVariables = sPageURL.split('&'),
        sParameterName,
        i;

    for (i = 0; i < sURLVariables.length; i++) {
        sParameterName = sURLVariables[i].split('=');

        if (sParameterName[0] === sParam) {
            return sParameterName[1] === undefined ? true : sParameterName[1];
        }
    }
};
// READ URL : used in user_profile & company master profile image
function readURL(input) {
    if (input.files && input.files[0]) {
        var reader = new FileReader();

        reader.onload = function (e) {
            $('#blah')
                .attr('src', e.target.result)
                .width(297)
                .height(297);
        };

        reader.readAsDataURL(input.files[0]);
    }
}

// $(document).ready(function(){
//     if($(".filter_buttons").hasClass("btn-filter-group")){
//         $(".btn-filter-group").addClass("jq-bl-0");
//     }
// });

// $(document).ready(function(){
//     $('.items a').on('click', function() {
//     var $this = $(this),
//         $bc = $('<div class="item"></div>');

//     $this.parents('li').each(function(n, li) {
//         var $a = $(li).children('a').clone();
//         $bc.prepend(' / ', $a);
//     });
//         $('.breadcrumb').html( $bc.prepend('<a href="#home">Home</a>') );
//        // return false;
//     });
// });

function getCurrentDate() {
    var d = new Date();

}

// Now any Date object can be declared
// let anyDate = new Date();
// and it can represent itself in the custom format defined above.
// console.log(anyDate.toShortFormat());    // 10-Jun-2018
Date.prototype.toShortFormat = function () {

    let monthNames = ["Jan", "Feb", "Mar", "Apr",
        "May", "Jun", "Jul", "Aug",
        "Sep", "Oct", "Nov", "Dec"
    ];

    let day = this.getDate();

    let monthIndex = this.getMonth();
    let monthName = monthNames[monthIndex];

    let year = this.getFullYear();

    return `${day}-${monthName}-${year}`;
}

//console.log(anyDate.toYMDFormat());    // 2018-08-28
Date.prototype.toYMDFormat = function () {

    let dd = this.getDate();
    var mm = this.getMonth() + 1;
    var yyyy = this.getFullYear();
    if (dd < 10) {
        dd = '0' + dd;
    }

    if (mm < 10) {
        mm = '0' + mm;
    }
    return `${yyyy}-${mm}-${dd}`;
}

// location - Generic Add Edit Blade
// $(document).ready(function () {

//     google.maps.event.addDomListener(window, 'load', initialize);
//     initialize();

//     function initialize() {
//         var options = {
//             componentRestrictions: { country: "IN" }
//         };
//         geocoder = new google.maps.Geocoder();
//         console.log(options);
//         var input = document.getElementById('autocomplete');
//         var autocomplete = new google.maps.places.Autocomplete(input, options);
//         autocomplete.addListener('place_changed', function () {
//             var place = autocomplete.getPlace();
//             $('#latitude').val(place.geometry['location'].lat());
//             $('#longitude').val(place.geometry['location'].lng());
//         });
//     }
//     //});

//     function errorFunction() {
//         console.log("Geocoder failed");
//     }
// })
