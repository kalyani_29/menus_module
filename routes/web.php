<?php

use Illuminate\Support\Facades\Route;

Route::get('/', function () {
    return view('welcome');
});



// ** Menu Route     **//

Route::resource('/menus', 'MenusController'); //this route call index n stored function.create the menu
Route::get('addMenuMappingItem', 'MenusController@addMenuMappingItem');//this Route add the menu in menu mapping

// ** Edit Menu Route **//
Route::get('editMenuMapping', 'MenusController@editMenuMapping');//This Route used to edit the menu
Route::post('/menus/update', 'MenusController@Update');//This Route used to update the menus.
Route::get('deleteMenuMappingAndRoles', 'MenusController@deleteMenuMappingAndRoles');//This Route used to deletet the menu in menumapping

Route::get('userAccessRole', 'MenusController@userAccessRole');
Route::post('save-user-access-role', 'MenusController@save_user_role_access');
Route::get('usermenuAccessTypes', 'MenusController@usermenuAccessTypes');

// ** menus dragdrop **//
Route::post('updateMenuList', 'MenusController@updateMenuList');


// **   menus report  **/
Route::get('menu_master_report', 'MenusReportController@index');//This Route used to create the menusreport.

/** * Export Excel */
Route::get('export_to_excel', 'ExportController@export');//This Route used to export the table data to excel sheet.





// Route::get('/menuList', 'MenusController@MenuList');
// Route::get('updateUserAccessRole', 'MenusController@updateUserAccessRole');
// Route::get('editUserAccessRole', 'MenusController@editUserAccessRole');
// Route::post('update_role_access', 'MenusController@updateRoleAccess');
// Route::get('change-role-access-by-chk', 'MenusController@update_association_role');


